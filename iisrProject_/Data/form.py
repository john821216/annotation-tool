from django.utils.translation import ugettext_lazy as _
from django import forms
from multipleFileInput import MultiFileField

class UploadTxtIndexForm(forms.Form):
    txtIndex = forms.FileField(
       #label='Select a file',
       #help_text='max. 42 megabytes'
    )
class UploadTxtsForm(forms.Form):
	txts = MultiFileField(min_num=0, max_num=10000, max_file_size=1024*1024*1024*1024)

class UploadTagInfoForm(forms.Form):
	tagInfo = forms.FileField(
		)
