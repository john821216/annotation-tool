from django.db import models

# Create your models here.
class Datas(models.Model):
    name_on_index = models.CharField(max_length = 25)
    file_parent = models.IntegerField(default=-1) 
    file_type = models.CharField(max_length = 25) 
    file_name = models.CharField(max_length = 25) 
    tagFile_name = models.CharField(max_length = 25)
  
class TxtIndexDocument(models.Model):
    txtIndexfile = models.FileField(upload_to='documents/')

class TxtIndex(models.Model):
	file_name = models.IntegerField(default=-1)
	node_name = models.CharField(max_length = 25)
	childIndex_from = models.IntegerField(default=-1)
	childIndex_to = models.IntegerField(default=-1)
	searchIndex = models.IntegerField(default=-1)

class Tags(models.Model):
	tag_name = models.CharField(max_length=50)
	tag_color = models.CharField(max_length=25)

class Txts(models.Model):
	txt_name = models.CharField(max_length= 100)
	txt_content = models.CharField(max_length= 100000)

class TagsInfo(models.Model):
	file_id = models.CharField(max_length= 30)
	offset_start = models.CharField(max_length=30)
	offset_end = models.CharField(max_length=30)
	tag_type = models.CharField(max_length=30)
	content = models.CharField(max_length=30)
	note = models.CharField(max_length=30)
	time = models.CharField(max_length=30)
	source= models.CharField(max_length=30)
