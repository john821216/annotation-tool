# -*- coding: utf-8 -*- 
from Data.models import Datas 
from Data.models import Tags
from Data.models import TagsInfo
from Data.models import TxtIndex
from Data.models import Txts
from django.shortcuts import render_to_response,RequestContext
from django import forms
from django.core import serializers
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from Data.models import TxtIndexDocument
from Data.form import UploadTxtsForm
from Data.form import UploadTxtIndexForm
from Data.form import UploadTagInfoForm
from datetime import datetime
from time import gmtime, strftime
import pytz
import urllib
import sqlite3
import re
import json
import csv
from django.utils.encoding import smart_str, smart_unicode
from django.contrib.auth.decorators import login_required
@login_required(login_url='/login')
def txtIndexManager(request):
	#get page 
	if request.GET.get('page') != None:
		curPage = int(request.GET.get('page'))
		print curPage
	else:
		curPage = 1

	if curPage == TxtIndex.objects.all().count()/ 100 +1:
		nextPage = '#'
	else :
		nextPage = curPage + 1

	if curPage == 1:
		previousPage = '#'
	else :
		previousPage = curPage -1
	limitPage = TxtIndex.objects.all().count() / 100 + 1
	pages = {"previousPage" : previousPage, "curPage": curPage , "nextPage":nextPage , "limitPage":limitPage}
	content = TxtIndex.objects.all()[(curPage-1)*100:(curPage-1)*100+100]

	datafield = ["file_name" , "node_name","childIndex_from", "childIndex_to", "searchIndex"]
	# Handle file upload
	print request
	if request.method == 'POST':
		print "57"
		indexForm = UploadTxtIndexForm(request.POST, request.FILES)
		#txtForm = UploadTxtsForm(request.POST, request.FILES)
		if indexForm.is_valid():
			#newdoc = TxtIndexDocument(txtIndexfile = request.FILES['txtIndexfile'])
			TxtIndex.objects.all().delete()
			request.FILES['txtIndex'].open('w+')
			text = request.FILES['txtIndex'].read()
			print text
			for data in text.split("\n"):
				newData =TxtIndex(file_name = data.split(",")[0],
					node_name = data.split(",")[1],
					childIndex_from = data.split(",")[2],
					childIndex_to = data.split(",")[3],
					searchIndex = data.split(",")[4],)
				newData.save();
				print data +" 71"
            # Redirect to the document list after POST
			return HttpResponseRedirect('/manager/')
	else:
		indexForm = UploadTxtIndexForm() # A empty, unbound form
		txtForm = UploadTxtsForm()
		print indexForm
		#print txtForm
		#print "64"

    # Load documents for the list page
	#documents = TxtIndexDocument.objects.all()
	#documents.delete();
	#print documents
    # Render list page with the documents and the form
	return render_to_response('txtIndex.html',{'indexForm': indexForm, 'txtForm':txtForm, 'headers' :datafield, 'contents' :content, 'pages' :pages},context_instance=RequestContext(request))

def uploadTxt(request):
	print "94"
	if request.method == 'POST':
		txtForm = UploadTxtsForm(request.POST, request.FILES)
		if txtForm.is_valid():
			txtList = request.FILES.getlist('txts')
			for txt in txtList:
				#request.FILES[afile].open('w+')
				txtContent = txt.read()
				#print txtContent
				#print txt.name
				txtName = txt.name.split(".")[0];
				newTxt = Txts.objects.filter(txt_name=txtName)
				if len(newTxt) == 0:
					newData = Txts(txt_name = txtName ,
								   txt_content = txtContent,)
					newData.save();
				else:
					newTxt.update(txt_name = txtName ,
								   txt_content = txtContent,)
					
			return HttpResponseRedirect('/manager/')
	else:
		#txtForm = UploadTxtsForm() # A empty, unbound form
		return HttpResponseRedirect('/manager/')

def addDataToFileDB(data):
	action = data.split("&")[0].split("=")[1]
	name = data.split("&")[1].split("=")[1]
	parent =data.split("&")[2].split("=")[1]
	f_type =data.split("&")[3].split("=")[1]
	file_name =data.split("&")[4].split("=")[1]
	tagFile_name =data.split("&")[5].split("=")[1]
	newData = Datas(name_on_index = name , 
					file_parent = int(parent),
					file_type = f_type,
					file_name = file_name,
					tagFile_name = tagFile_name,
					)
	newData.save()

def deleteData(data) :
	action = data.split("&")[1].split("=")[1]
	name = data.split("&")[2].split("=")[1]
	parent =data.split("&")[3].split("=")[1]
	f_type =data.split("&")[4].split("=")[1]
	file_name =data.split("&")[5].split("=")[1]
	tagFile_name =data.split("&")[6].split("=")[1]
	print name + " " + parent + " " + f_type +" " + file_name +" " + tagFile_name
	delData = Datas.objects.get(name_on_index = name , 
					file_parent = int(parent),
					file_type = f_type,
					file_name = file_name,
					tagFile_name = tagFile_name,
					)
	delData.delete()

def editData(data):
	print len(data.split("&"))
	name = data.split("&")[1].split("=")[1]
	parent =data.split("&")[2].split("=")[1]
	f_type =data.split("&")[3].split("=")[1]
	file_name =data.split("&")[4].split("=")[1]
	tagFile_name =data.split("&")[5].split("=")[1]
	e_name = data.split("&")[6].split("=")[1]
	e_parent =data.split("&")[7].split("=")[1]
	e_f_type =data.split("&")[8].split("=")[1]
	e_file_name =data.split("&")[9].split("=")[1]
	e_tagFile_name =data.split("&")[10].split("=")[1]
	#print Datas.object.all() +"83"
	print name +" "+ parent
	print e_name +" " + e_parent +" " + e_f_type + " "+ e_file_name + " " + e_tagFile_name
 	editData =  Datas.objects.filter(name_on_index = e_name, 
					file_parent = int(e_parent),
					file_type = e_f_type,
					file_name = e_file_name,
					tagFile_name = e_tagFile_name,
					)

	editData.update(name_on_index = name , 
					file_parent = int(parent),
					file_type = f_type,
					file_name = file_name,
					tagFile_name = tagFile_name,
					)
@login_required(login_url='/login')
def tagManager(request):
	content = Tags.objects.all()
	#Datas.objects.all().delete()
	#for data in content:
		#print data.name_on_index
		#print data.file_type
		#print data.file_name
		#print data.tagFile_name
	datafield = ["tag_name" , "tag_color"]
	#print request.body +"20"
	if request.method == 'POST':
		print request.POST.get('action','') 	
		if request.POST.get('action','') == 'delete':
			print "DELETE"
			deleteTagData(urllib.unquote(request.body))
			return HttpResponseRedirect('/manager/')
		elif request.POST.get('action', '') == 'add':
			print "ADD"
			addTagData(urllib.unquote(request.body))
			return HttpResponseRedirect('/manager/')
		elif request.POST.get('action', '') == 'edit':
			print "EDIT"
			editTagData(urllib.unquote(request.body))
			return HttpResponseRedirect('/manager/')
			#print request.body
	else :
		return render_to_response('tag.html' , {'headers' :datafield, 'contents' :content} ,context_instance=RequestContext(request))

def addTagData(data):
	action = data.split("&")[0].split("=")[1]
	name = data.split("&")[1].split("=")[1]
	color =data.split("&")[2].split("=")[1]
	newData = Tags(tag_name = name , 
					tag_color = color,
					)
	newData.save()

def deleteTagData(data) :
	action = data.split("&")[1].split("=")[1]
	name = data.split("&")[2].split("=")[1]
	color =data.split("&")[3].split("=")[1]
	print data

	delData = Tags.objects.get(tag_name = name , 
					tag_color = color,
					)
	delData.delete()

def editTagData(data):
	name = data.split("&")[1].split("=")[1]
	color =data.split("&")[2].split("=")[1]
	e_name = data.split("&")[3].split("=")[1]
	e_color =data.split("&")[4].split("=")[1]
	
	print e_name +" " + e_color + " " + name + " " + color
 	editData =  Tags.objects.filter(tag_name= e_name, 
					tag_color = e_color,
					)

	editData.update(tag_name = name,
					tag_color = color,
					)
def export_to_csv(request):
	queryset = TagsInfo.objects.all()
	fields = ('file_id','offset_start' ,'offset_end','tag_type','content','source')
	response = HttpResponse( content_type='text/plain')
	response['Content-Disposition'] = 'attachment; filename=output.txt'
	writer = csv.writer(response)
	for obj in queryset:
		writer.writerow([obj.file_id.encode('utf-8') +"\t" +obj.offset_start.encode('utf-8') +"\t" + obj.offset_end.encode('utf-8')  +"\t" + obj.tag_type.encode('utf-8') +"\t"+ obj.content.encode('utf-8') + "\t"+obj.source.encode('utf-8')])
	return response

@login_required(login_url='/login')
def tagInfoManager(request):
	#get current page
	if request.GET.get('page') != None:
		curPage = int(request.GET.get('page'))
		print curPage
	else:
		curPage = 1

	print (curPage-1)*100
	content = TagsInfo.objects.all()[(curPage-1)*100:(curPage-1)*100+100]
	limitPage = TagsInfo.objects.all().count() / 100 + 1
	if curPage == TagsInfo.objects.all().count()/ 100 +1:
		nextPage = '#'
	else :
		nextPage = curPage + 1

	if curPage == 1:
		previousPage = '#'
	else :
		previousPage = curPage -1

	pages = {"previousPage" : previousPage, "curPage": curPage , "nextPage":nextPage , "limitPage":limitPage}
	datafield = ["file_id" , "offset_start","offset_end", "tag_type", "content","note","time","source"]
	#print request.body +"20"
	if request.method == 'POST':
		print request.path
		if request.POST.get('action','') == 'delete':
			print "DELETE"
			deleteTagInfo(urllib.unquote(request.body))
			return HttpResponseRedirect('/manager/tagInfo.html')
		elif request.POST.get('action', '') == 'add':
			print "ADD"
			addTagInfoToDB(urllib.unquote(request.body))
			return HttpResponseRedirect('/manager/tagInfo.html')
		elif request.POST.get('action' ,'') == 'deleteFileIdAndSourceDataLength':
			length = deleteFileIdAndSourceDataLength(urllib.unquote(request.body))
			return HttpResponse(json.dumps({'length' : length}), content_type='application/json') 
		elif request.POST.get('action' ,'') == 'deleteFileIdAndSource':
			deleteFileIdAndSource(urllib.unquote(request.body))
			return HttpResponseRedirect('/manager/tagInfo.html')
		#elif request.POST.get('action', '') == 'exportTxtFile':
			
		#	return export_to_csv(request, tagsInfo, fields = ('file_id','offset_start' ,'offset_end','tag_type','content','source'))
		#file upload
		elif request.path == '/manager/tagInfo/':
			tagInfoForm = UploadTagInfoForm(request.POST, request.FILES)
			if tagInfoForm.is_valid():
				request.FILES['tagInfo'].open('w+')
				text = request.FILES['tagInfo'].read()
				print text
				for data in text.split("\n"):
					file_id = re.split(r'\t+', data)[0]
					offset_start = re.split(r'\t+', data)[1].split(",")[0]
					offset_end=re.split(r'\t+', data)[1].split(",")[1]
					tag_type = re.split(r'\t+', data)[2]
					content = re.split(r'\t+', data)[3]
					source = re.split(r'\t+', data)[4]

					local_tz = pytz.timezone('Asia/Taipei')	
					time = datetime.utcnow().replace(tzinfo=pytz.utc).astimezone(local_tz).strftime('%Y-%m-%d %H:%M:%S')
					if len(re.split(r'\t+', data))== 6 :
						note = data.split("\t")[5]
					else :
						note = ''
					#print file_id + " " + offset_start +"  " + offset_end +" " +tag_type+" " +content +" "+ source +" " +time+" " + source
						
					newTagInfo = TagsInfo.objects.filter(file_id = file_id,
										offset_start = offset_start,
										offset_end = offset_end,
										tag_type = tag_type,
										content = content,
										source = source,
										)
					if len(newTagInfo) == 0:
						newTagInfo = TagsInfo(file_id = file_id,
									offset_start = offset_start,
									offset_end = offset_end,
									tag_type = tag_type,
									content = content,
									note = note,
									time = time,
									source = source,
									)
				
						newTagInfo.save();			
			return HttpResponseRedirect('/manager/tagInfo.html')
	else :
		tagInfoForm = UploadTagInfoForm()
		return render_to_response('tagInfo.html' , {'headers' :datafield, 'contents' :content , 'tagInfoForm' :tagInfoForm , 'pages' :pages}, context_instance=RequestContext(request))

def addTagInfoToDB(data):
	action = data.split("&")[0].split("=")[1]
	file_id = data.split("&")[1].split("=")[1]
	offset_start =data.split("&")[2].split("=")[1]
	offset_end = data.split("&")[3].split("=")[1]
	tag_type = data.split("&")[4].split("=")[1]
	content = data.split("&")[5].split("=")[1]
	note = data.split("&")[6].split("=")[1]
	time  = data.split("&")[7].split("=")[1]
	source = data.split("&")[8].split("=")[1]
	newData = TagsInfo(file_id = file_id,
					offset_start = offset_start,
					offset_end = offset_end,
					tag_type = tag_type,
					content = content,
					note = note,
					time = time,
					source = source,
					)
	newData.save()

def deleteTagInfo(data) :
	print data
	action = data.split("&")[1].split("=")[1]
	file_id = data.split("&")[2].split("=")[1]
	offset_start =data.split("&")[3].split("=")[1]
	offset_end = data.split("&")[4].split("=")[1]
	tag_type = data.split("&")[5].split("=")[1]
	content = data.split("&")[6].split("=")[1]
	note = data.split("&")[7].split("=")[1]
	time  = data.split("&")[8].split("=")[1].split("+")[0] + " "+ data.split("&")[8].split("=")[1].split("+")[1]
	source = data.split("&")[9].split("=")[1]
	print source
	print action +" " + file_id +" " + offset_start +" " + offset_end +" " +tag_type +" " +content +" " + note +" " + time+" " +source+"A" 
	delData =  TagsInfo.objects.get(file_id = file_id,
					offset_start = offset_start,
					offset_end = offset_end,
					tag_type = tag_type,
					content = content,
					note = note,
					time = time,
					source = source,
					)
	delData.delete()

def deleteFileIdAndSourceDataLength(data):
	#print data
	action = data.split("&")[1].split("=")[1]
	file_id = data.split("&")[2].split("=")[1]
	source = data.split("&")[3].split("=")[1]

	if file_id != '' or source != '':
		print "386"
		if file_id == '':
			delData = TagsInfo.objects.filter(source = source,)
			return delData.count()
			#delData.delete()
		elif source == '':
			delData = TagsInfo.objects.filter(file_id = file_id,)
			return delData.count()
			#delData.delete()	
		else:
			delData =  TagsInfo.objects.filter(file_id = file_id,
									source = source,
						)		
			return delData.count()		
			#delData.delete()

def deleteFileIdAndSource(data):
	#print data
	action = data.split("&")[1].split("=")[1]
	file_id = data.split("&")[2].split("=")[1]
	source = data.split("&")[3].split("=")[1]

	if file_id != '' or source != '':
		print "386"
		if file_id == '':
			delData = TagsInfo.objects.filter(source = source,)
			delData.delete()
		elif source == '':
			delData = TagsInfo.objects.filter(file_id = file_id,)
			delData.delete()	
		else:
			delData =  TagsInfo.objects.filter(file_id = file_id,
									source = source,
						)		
			delData.delete()


