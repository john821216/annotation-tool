(function() {
    var file_id;
    var offset_start;
    var offset_end;
    var tag_type;
    var content;
    var note;
    var time;
    var source;
    var tag_color;
    var isEdit;
    var source;

    var edit_note;
    var edit_tag_type;
    showText();
    
    document.getElementById("closeBtn").addEventListener("click", function() {
        window.parent.changeContent.handlePopupMessage("closePopup","");
    }, false);

    document.getElementById("confirmBtn").addEventListener("click", function() {
        if(isEdit == 'false'){
            var sendObj = new Object();
            sendObj.start = offset_start;
            sendObj.end = offset_end;
            sendObj.color = tag_color;
            sendObj.tagText = tag_type;
            sendObj.source = "web";
            window.parent.changeContent.handlePopupMessage("confirmOK", JSON.stringify(sendObj));
            saveTagAttribute();
        } else {
            //console.log("30");
            window.parent.changeContent.handlePopupMessage("edit", "");
            editTagAttribute();
        }
       


       
    }, false);
    
    function clickEditTagItemListener(event){
        event.stopPropagation();
        var targetNumber = event.target.number;
        //if(typeof targetNumber != 'undefined'){
        //console.log(event.target);
        //console.log(targetNumber);

        tag_color = document.getElementsByClassName('tagItem')[targetNumber].childNodes[1].style.color;
        tag_type  = document.getElementsByClassName('tagItem')[targetNumber].childNodes[1].innerHTML ||
                   document.getElementsByClassName('tagItem')[targetNumber].childNodes[1].innerText;
    }

    function dbclickEditTagItemListener(event){
        event.stopPropagation();
        var targetNumber = event.target.number;
        tag_color = document.getElementsByClassName('tagItem')[targetNumber].childNodes[1].style.color;
        tag_type  = document.getElementsByClassName('tagItem')[targetNumber].childNodes[1].innerHTML ||
                   document.getElementsByClassName('tagItem')[targetNumber].childNodes[1].innerText;
        if(isEdit == 'false'){
            var sendObj = new Object();
            sendObj.start = offset_start;
            sendObj.end = offset_end;
            sendObj.color = tag_color;
            sendObj.tagText = tag_type;
            sendObj.source = "web";
            window.parent.changeContent.handlePopupMessage("confirmOK", JSON.stringify(sendObj));
            saveTagAttribute();
        } else {
            //console.log("30");
            window.parent.changeContent.handlePopupMessage("edit", "");
            editTagAttribute();
        }
    }
   
   function showText(){ 
        content = localStorage.getItem('content');
        offset_start = localStorage.getItem('offset_start');
        offset_end = localStorage.getItem('offset_end');
        isEdit = localStorage.getItem('isEdit');
        //console.log(isEdit);
        document.getElementsByClassName('tagText')[0].innerText = content;
        document.getElementsByClassName('tagText')[0].innerHTML = content;   
    }

    function editTagAttribute(){
        $.ajax({
            type: "POST",      
            data:  { csrfmiddlewaretoken: getCookie('csrftoken'),
                    action : 'editTagAttribute',
                    file_id:  localStorage.getItem('file_id'),
                    offset_start: offset_start,
                    offset_end: offset_end,
                    tag_type: localStorage.getItem('tag_type'),
                    source: localStorage.getItem('source'),
                    content:content,
                    edit_tag_type : tag_type,
                    edit_note : document.getElementsByName("note")[0].value,
                    time: getCurrentTime(),
                    source: "web",
                    },
                    success: function(response) { // on success..
                        //console.log(response);  
                        window.location.reload();
                    },
                    error: function(err, x, req) { // on error..
                        //console.log('my message ' + err + " "+ req); 
                    }
        }); 
    }
    function saveTagAttribute(){
        //offset is 9
        $.ajax({
            type: "POST",      
            data:  { csrfmiddlewaretoken: getCookie('csrftoken'),
                    action : 'saveTagAttribute',
                    file_id:  localStorage.getItem('file_id'),
                    offset_start:offset_start-9,
                    offset_end: offset_end-9,
                    tag_type:tag_type,
                    content:content,
                    note: document.getElementsByName("note")[0].value,
                    time: getCurrentTime(),
                    source: "web",
                    },
                    success: function(response) { // on success..
                        //console.log(response);  
                    },
                    error: function(err, x, req) { // on error..
                        //console.log('my message ' + err + " "+ req); 
                    }
        }); 
    }

    function getCurrentTime(){
        var d= new Date();
        var fullYear = d.getFullYear();
        var month = d.getMonth()+1;
        var date = d.getDate();

        var hour = d.getHours();
        var minute  = d.getMinutes();
        var second = d.getSeconds();

        return fullYear +"-"+month +"-"+date +" "+ hour+":"+minute+":"+ second;
    }
    
    var tagItem = document.getElementsByClassName('tagItem');
    for(var i = 0 ; i < tagItem.length ; i++){
        tagItem[i].addEventListener("click" , clickEditTagItemListener ,false);
        tagItem[i].number = i;
    }

    for(var i = 0 ; i < tagItem.length ;i++){
        tagItem[i].addEventListener("dblclick" , dbclickEditTagItemListener, false);
        tagItem[i].number = i;
    }

    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

})();
