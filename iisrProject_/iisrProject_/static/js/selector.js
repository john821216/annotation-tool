var Selector = (function() {
    var targetTagTypeHTML="";
    return{
        ini : function(){
            getTagType();
        },
    };

    function getTagType(){
        $.ajax({
            type: "POST",      
            data:   { csrfmiddlewaretoken: getCookie('csrftoken'),
                    action : 'getTagType',
                    },
                    success: function(response) { // on success..
                        for(var i = 0 ; i < response.length ; i++){
                            var tag_type = response[i]['fields']['tag_name'];
                            var tag_color = response[i]['fields']['tag_color'];
                            targetTagTypeHTML += "<label for=tag"+
                                                (i+1)+">"+
                                                "<input type='checkbox'" +" id='checkbox"+(i+1)+"'/>"+
                                                "<span style='color:rgb("+tag_color+");'>  "+
                                                tag_type+"  </span></label><br>"
                        }
                        document.getElementById('tagSelectorInfo').innerHTML =  targetTagTypeHTML;
                    },
                    error: function(err, x, req) { // on error..
                        //console.log('my message ' + err + " "+ req); 
                    }
        });
    }

    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            //console.log(cookies);
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
})();