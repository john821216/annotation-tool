from django.conf.urls import include, url
from django.contrib import admin
#from Data.views import txtIndexmanager
from Data.views import tagManager
from Data.views import tagInfoManager
from iisrProject_.views import mainPage
from iisrProject_.views import tagPage
from Data.views import txtIndexManager
from Data.views import uploadTxt
from iisrProject_.views import loginPage
from iisrProject_.views import authLogin
from django.conf.urls.static import static
from django.conf import settings
from django.views.generic.base import RedirectView
from Data.views import export_to_csv
urlpatterns = [
    #url(r'^admin/', include(admin.site.urls)),
    url('^manager/txtIndex.html', txtIndexManager),
    url('^manager/$', RedirectView.as_view(url='txtIndex.html', permanent=False), name='txtIndex'),
    url('^manager/tag.html' , tagManager),
    url('^manager/tagInfo.html' , tagInfoManager),
    url('^manager/fileIndex/$', txtIndexManager, name='txtIndexManager'),
    url('^manager/tagInfo/$' , tagInfoManager ,name = 'tagInfoManager'),
    url('^manager/txt/$' , uploadTxt, name='uploadTxt'),
    url('^media/output.txt', export_to_csv),
    url('^login/$', loginPage),
    url('^auth/$' ,authLogin),
    url(r'^$', RedirectView.as_view(url='index', permanent=False), name='index'),
    url(r'^index',mainPage),
    url(r'^tag', tagPage),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


