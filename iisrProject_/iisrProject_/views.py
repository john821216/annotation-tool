from django.http import HttpResponse
from django.template.loader import get_template 
from django.shortcuts import render_to_response,RequestContext
from django.template import Context
from Data.models import Datas
from Data.models import Tags
from Data.models import Txts
from Data.models import TagsInfo
from Data.models import TxtIndex
from django.core.context_processors import csrf
from django.core import serializers
from django.contrib.auth import authenticate,login
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

import sqlite3
import datetime
import json

def loginPage(request):
	loginError= False
	if request.user.is_authenticated():
		return HttpResponseRedirect('/index')
	else :
		return render_to_response('login.html',{'loginError': loginError},context_instance=RequestContext(request))

def authLogin(request):
	username = request.POST.get('username','')
	password = request.POST.get('password','')
	user = authenticate(username=username, password=password)
	if user is not None:
		if user.is_active:
			login(request,user)
			return HttpResponseRedirect('/index')
		else:
			loginError = True
			return render_to_response('login.html',{'loginError': loginError},context_instance=RequestContext(request))
	else:
		loginError = True
		return render_to_response('login.html',{'loginError': loginError},context_instance=RequestContext(request))
@login_required(login_url='/login')
def mainPage(request):
	fileContent = Datas.objects.all()
	
	if request.method == 'POST' and request.is_ajax():
		#if request.POST.get('action') == 'iniDataGet':
		#	return HttpResponse(serializers.serialize('json', fileContent), content_type='application/json')
		if request.POST.get('action') =='getTxtContent':
			file_name = request.POST.get('file_name')
			file_content = Txts.objects.get(txt_name = file_name)
			#print txtContent.txt_content
			return HttpResponse(file_content.txt_content)
		elif request.POST.get('action') =='getTagContent':
			file_name = request.POST.get('file_name')
			#file_name = Datas.objects.get(name_on_index = id).file_name
			tagInfo = TagsInfo.objects.filter(file_id = file_name)
			return HttpResponse(serializers.serialize('json',tagInfo), content_type='application/json')
		elif request.POST.get('action') =='getTagColor':
			tag_name = request.POST.get('tag_type')
			tag_color = Tags.objects.get(tag_name = tag_name).tag_color
			return HttpResponse(tag_color)
		elif request.POST.get('action') == 'saveTagAttribute':
			file_id = request.POST.get('file_id')
			offset_start = request.POST.get('offset_start')
			offset_end = request.POST.get('offset_end')
			tag_type = request.POST.get('tag_type')
			content = request.POST.get('content')
			note = request.POST.get('note')
			time = request.POST.get('time')
			source = request.POST.get('source')
			filterTag = TagsInfo.objects.filter(file_id= file_id,
						offset_start = offset_start,
						offset_end = offset_end,
						tag_type = tag_type,
						source = source)

			if len(filterTag) == 0:
				newData = TagsInfo(file_id= file_id,
							offset_start = offset_start,
							offset_end = offset_end,
							tag_type = tag_type,
							content = content,
							note = note,
							time = time,
							source = source,
						)
				newData.save()
				return HttpResponse("Save successfully")
			else :
				return HttpResponse("overlap")
		elif request.POST.get('action') == 'editTagAttribute':
			file_id = request.POST.get('file_id')
			offset_start = request.POST.get('offset_start')
			offset_end = request.POST.get('offset_end')
			tag_type = request.POST.get('tag_type')
			content = request.POST.get('content')
			time = request.POST.get('time')
			source = request.POST.get('source')	
			edit_tag_type = request.POST.get('edit_tag_type')
			edit_note =request.POST.get('edit_note')	
			filterTag = TagsInfo.objects.filter(file_id= file_id,
						offset_start = offset_start,
						offset_end = offset_end,
						tag_type = tag_type,
						source = source)

			print file_id +" " + offset_start +" " + offset_end +" " +tag_type +" " + source
 			filterTag.update(file_id= file_id,
							offset_start = offset_start,
							offset_end = offset_end,
							tag_type = edit_tag_type,
							content = content,
							note = edit_note,
							time = time,
							source = source,
							)
			return HttpResponse("Alter successfully")
		elif request.POST.get('action') == 'deleteTag':
			file_id = request.POST.get('file_id')
			offset_start = request.POST.get('offset_start')
			offset_end = request.POST.get('offset_end')
			tag_type = request.POST.get('tag_type')
			source = request.POST.get('source')
			delData = TagsInfo.objects.get(file_id= file_id,
						offset_start = offset_start,
						offset_end = offset_end,
						tag_type = tag_type,
						source = source,
					)
			delData.delete()
			return HttpResponse("Delete successfully" );
		elif request.POST.get('action') == 'showTagInfo':
			file_id = request.POST.get('file_id')
			offset_start = request.POST.get('offset_start')
			offset_end = request.POST.get('offset_end')
			tag_type = request.POST.get('tag_type')
			source = request.POST.get('source')

			tagInfo = TagsInfo.objects.filter(file_id= file_id,
						offset_start = offset_start,
						offset_end = offset_end,
						tag_type = tag_type,
						source = source,
					)
			print tagInfo
			return HttpResponse(serializers.serialize('json',tagInfo), content_type='application/json')
		elif request.POST.get('action') == 'getTagType':
			tagContent = Tags.objects.all()
			return HttpResponse(serializers.serialize('json',tagContent), content_type='application/json')
		elif request.POST.get('action') == 'getSideBarItem':
			searchIndex = request.POST.get('searchIndex')
			sideBarItem = TxtIndex.objects.filter(searchIndex = searchIndex)
			return HttpResponse(serializers.serialize('json',sideBarItem), content_type='application/json') 
		#elif request.POST.get('action') == 'getFile':
		#	file_id = request.GET.get('file_id')
		#	return HttpResponse(json.dumps({'file_id' : file_id}), content_type='application/json') 
	else:
		return render_to_response('index.html', context_instance=RequestContext(request))
@login_required(login_url='/login')
def tagPage(request):
	tagContent = Tags.objects.all()
	print tagContent
	return render_to_response('selectTxt.html' , {'tagContents' :tagContent} ,context_instance=RequestContext(request))