var changeContent = (function() {
    var line_start_x;
    var data_id = 0;
    var line_start_y;
    var start_x = 0;
    var start_y = 20;
    var lineCount = 0;
    var txt;
    var hasMouseListener = false;
    var wordSpacingLength = 5;
    var needNewLineToAppendText = false;
    var maxXPerLine = 810;
    var currentLine = 0;
    var fileloadingFinish = false;
    var fileExist = false;
    var lineSpacing = 40;
    var tagid = 0;
    var delBtnAppear = false;
    var popup;
    var dialog;
    var currentTagNumber = 0;
    var tagInfoJson;
    var highlightTag =[];
    var leftOffset =3;
    return {
        change : function(command, data) {
            if (command == "text") {
                fileloadingFinish = false;
                currentLine =0;
                readContent(data);
                txt = data;
               
            } else if (command == "tag") {
                currentTagNumber =0;
                tagInfoJson = data;
                getTagColorAndTagText();
            }
        },

        handlePopupMessage : function(mes, data) {          
            if (mes == "confirmOK") {
                var data = JSON.parse(data);
                dialog.close();
                highlightText((parseInt)(data.start), (parseInt)(data.end), data.color , data.tagText , data.source);
                //console.log("highlightText");
            } else if (mes == "closePopup") {
                //console.log("Close");
                dialog.close();
            } else if( mes == 'edit'){
                //console.log("50");
                dialog.close();
            }
        }
    };

    function setSVGHeight(){
        var textHeight = document.getElementsByClassName('text')[0].getBBox().height;
        document.getElementById('svgContent').style.height = (textHeight +1) +'px';
    }

    function checkFileFinishLoadingThenHighlightText(start, end, color,tagText , source) {
        if (fileloadingFinish == false) {
            //console.log("REloading");
            window.setTimeout(function() {
                checkFileFinishLoadingThenHighlightText(start, end, color,tagText , source);
            }, 1000);
        } else {
            highlightText(start, end, color,tagText, source);
        }
    }

    function getTagColorAndTagText(){
        //console.log(tagInfoJson);
        if(tagInfoJson.length > 0){
            var start = (parseInt)(tagInfoJson[currentTagNumber]['fields']['offset_start']) + 9;
            var end = (parseInt)(tagInfoJson[currentTagNumber]['fields']['offset_end']) + 9;
            var tagText = tagInfoJson[currentTagNumber]['fields']['tag_type'];
            var source = tagInfoJson[currentTagNumber]['fields']['source'];
            var color;
            //console.log(start +" " + end +" "+ tagText +' ' + source + "    22222");
            $.ajax({
                type: "POST",      
                data:  { csrfmiddlewaretoken: getCookie('csrftoken'),
                         action : 'getTagColor',
                         tag_type : tagInfoJson[currentTagNumber]['fields']['tag_type'],
                },
                //dataType: "json",
                success: function(response) { // on success..
                    //console.log("61 " + response)
                    color = "rgb("+response+")";
                    checkFileFinishLoadingThenHighlightText(start, end, color,tagText , source);
                    currentTagNumber++;
                    if(currentTagNumber < tagInfoJson.length){
                       getTagColorAndTagText();
                    }
                },
                error: function(err, x, req) { // on error..
                    //console.log('my message ' + err + " "+ req); 
                }
            });
        }
    }

    function readContent(text){
        emptyContent();
        loadingStart();
        addTextToSVG(text);

        if (!hasMouseListener) {
            addMouseListener();
            hasMouseListener = true;
        }
    }

    function emptyContent(){
         //empty first
        $(".text").empty();
        $(".highlight").empty();
        $(".tag").empty();
	$(".tagRect").empty();
    }

    function loadingStart(){
         document.getElementById("loading").style.display = "block";
    }

    function tagmouseoverEvent(evt) {
        var x = evt.target.getAttribute("x");
        var y = evt.target.getAttribute("y");
        var width = evt.target.getAttribute("width");
        var tagid = evt.target.getAttribute("tagid");
        var line = evt.target.getAttribute("line");
        var tagLine = evt.target.getAttribute("tagLine");
        var radius = 10;

        //del button
        document.getElementById("delButton").setAttribute("x", (parseFloat)(x) + (parseFloat)(width) );
        document.getElementById("delButton").setAttribute("y", (parseFloat)(y)- 7);
        document.getElementById("delButton").setAttribute("tagY", y);
        document.getElementById("delButton").setAttribute("tagid", tagid);
        document.getElementById("delButton").setAttribute("line", line);
        document.getElementById("delButton").setAttribute("tagLine", tagLine);
        document.getElementById("delButton").style.display = "block";

        //
        evt.target.setAttribute("stroke-width" , 3);
        /*
        var highlightNodes = document.getElementsByClassName('highlight')[0].querySelectorAll('[tagId='+ tagId +']');
        for(var currentNode =0 ; currentNode < highlightNodes.length ; currentNode++){
                console.log("145  55");
                var x = highlightNodes[currentNode].getAttribute("x");
                var y = highlightNodes[currentNode].getAttribute("y");
                var offset_start = highlightNodes[currentNode].getAttribute("offset_start");
                var offset_end = highlightNodes[currentNode].getAttribute("offset_end");
                var color = highlightNodes[currentNode].getAttribute("fill");
                var width = highlightNodes[currentNode].getAttribute("width");
                var height = currentNode.getAttribute("height");
                var currentLine = highlightNodes[currentNode].getAttribute("currentLine");
                var tagText = highlightNodes[currentNode].getAttribute("tagText");
                var tagId = highlightNodes[currentNode].getAttribute("tagId");
                var source = highlightNodes[currentNode].getAttribute("source");

                var tempTag = document.createElementNS("http://www.w3.org/2000/svg", "rect");
                tempTag.setAttributeNS(null, "x", x);
                tempTag.setAttributeNS(null, "y", y);
                tempTag.setAttributeNS(null, "offset_start" , offset_start);
                tempTag.setAttributeNS(null, "offset_end" , offset_end);
                tempTag.setAttributeNS(null, "fill", color);
                tempTag.setAttributeNS(null, "fill-opacity", 0.5);
                tempTag.setAttributeNS(null, "width", width);
                tempTag.setAttributeNS(null, "height", 10);
                tempTag.setAttributeNS(null, "currentLine", currentLine);
                tempTag.setAttributeNS(null, "tagText" , tagText);
                tempTag.setAttributeNS(null, "tagId", tagId);
                tempTag.setAttributeNS(null, "source" , source);

                var textNode = document.createTextNode("");
                tempTag.appendChild(textNode);
                document.getElementsByClassName("highlight")[0].appendChild(tempTag);
                highlightTag.push(tempTag);
        }
        */
        var length = document.getElementsByClassName("highlight")[0].childNodes.length;
        for(var currentTextCount = 0 ; currentTextCount < length; currentTextCount++) {
            //console.log(currentTextCount +  "  " +document.getElementsByClassName("highlight")[0].childNodes.length);
            if (document.getElementsByClassName("highlight")[0].childNodes[currentTextCount].getAttribute("tagid") == tagid) {
                var node = document.getElementsByClassName("highlight")[0].childNodes[currentTextCount]
                var x = node.getAttribute("x");
                var y = node.getAttribute("y");
                var offset_start = node.getAttribute("offset_start");
                var offset_end = node.getAttribute("offset_end");
                var color = node.getAttribute("fill");
                var width = node.getAttribute("width");
                var height = node.getAttribute("height");
                var currentLine = node.getAttribute("currentLine");
                var tagText = node.getAttribute("tagText");
                var tagid = node.getAttribute("tagid");
                var source = node.getAttribute("source");

                var tempTag = document.createElementNS("http://www.w3.org/2000/svg", "rect");
                tempTag.setAttributeNS(null, "x", x);
                tempTag.setAttributeNS(null, "y", y);
                //newText.setAttributeNS(null, "highlight-range", range);
                tempTag.setAttributeNS(null, "offset_start" , offset_start);
                tempTag.setAttributeNS(null, "offset_end" , offset_end);
                tempTag.setAttributeNS(null, "fill", color);
                tempTag.setAttributeNS(null, "fill-opacity", 0.5);
                tempTag.setAttributeNS(null, "width", width);
                tempTag.setAttributeNS(null, "height", 10);
                tempTag.setAttributeNS(null, "currentLine", currentLine);
                tempTag.setAttributeNS(null, "tagText" , tagText);
                tempTag.setAttributeNS(null, "tagid", tagid);
                tempTag.setAttributeNS(null, "source" , source);

                var textNode = document.createTextNode("");
                tempTag.appendChild(textNode);
                document.getElementsByClassName("highlight")[0].appendChild(tempTag);
                highlightTag.push(tempTag);
                //currentTextCount= 0;
            }
        }

    }

    function tagmouseoutEvent(evt) {
        //remove all
        for(var curTag = 0; curTag < highlightTag.length ; curTag++){
            highlightTag[curTag].remove();
        }
        evt.target.setAttribute("stroke-width" , 2);
    }

    function tagmouseclickEvent(evt){
        var tagid =  evt.target.getAttribute("tagid");
        for(var currentTextCount = 0 ; currentTextCount <  document.getElementsByClassName("highlight")[0].childNodes.length; currentTextCount++) {
            if (document.getElementsByClassName("highlight")[0].childNodes[currentTextCount].getAttribute("tagid") == tagid) {
                var node = document.getElementsByClassName("highlight")[0].childNodes[currentTextCount]
                var offset_start = node.getAttribute("offset_start");
                var offset_end = node.getAttribute("offset_end");
                var tag_type = node.getAttribute("tagText");
                var source = node.getAttribute("source");
                showTagInfo(offset_start , offset_end , tag_type , source);
                break;
                //currentTextCount= 0;
            }
        }
    }
    
    function tagmousedbclickEvent(evt){
        var tagid =  evt.target.getAttribute("tagid");
        //console.log(tagid);
        var node = document.getElementsByClassName('highlight')[0].querySelector("[tagid='" +tagid +"']");
        //console.log(node);
        var offset_start = node.getAttribute("offset_start");
        var offset_end = node.getAttribute("offset_end");
        var tag_type =  node.getAttribute("tagText");
        var source =node.getAttribute("source");
        localStorage.setItem("offset_start", offset_start);
        localStorage.setItem("offset_end" , offset_end);
        localStorage.setItem("isEdit" , true);
        localStorage.setItem("content" , txt.replace(/\r?\n/g,"").replace(/ /g,"").substring(offset_start,offset_end));
        localStorage.setItem("tag_type" , tag_type);
        localStorage.setItem("source" , source);
        //console.log(offset_start + " " + offset_end);
        //open dialog
        dialog = new BootstrapDialog({
            message: function(dialog) {
                var $message = $('<div></div>');
                //default
                var pageToLoad = dialog.getData('pageToLoad');
                    $message.load(pageToLoad);
                
                    return $message;
                },
                data: {
                    'pageToLoad': 'tag'
                }          
            });
            dialog.realize();
            dialog.getModalHeader().hide();
            dialog.open();
    }

    function showTagInfo(offset_start , offset_end , tag_type, source){
        //console.log(offset_start +" " + offset_end + " " + tag_type +" " + source);
        $.ajax({
            type: "POST",      
            data:  { csrfmiddlewaretoken: getCookie('csrftoken'),
                    action : 'showTagInfo',
                    file_id:  localStorage.getItem('file_id'),
                    offset_start:offset_start,
                    offset_end: offset_end,
                    tag_type:tag_type,
                    source:  source,
                    },
                    success: function(response) { // on success..
                        //console.log("Get successfully"); 
                        //console.log(response);
                        var offset_start = (parseInt)(response[0]['fields']['offset_start']) + 9;
                        var offset_end = (parseInt)(response[0]['fields']['offset_end']) + 9;
                        var tagType =response[0]['fields']['tag_type'];
                        var content = response[0]['fields']['content'];
                        var note =response[0]['fields']['note'];
                        var time =response[0]['fields']['time'];
                        var source = response[0]['fields']['source'];
                        document.getElementById('detail').innerHTML = "<table>"
                        +"<tr><td>offset_start</td><td>"+(parseInt)(offset_start-9)+"</td></tr>"
                        +"<tr><td>offset_end</td><td>"+(parseInt)(offset_end-9)+"</td></tr>"
                        +"<tr><td>tagType</td><td>"+tag_type+"</td></tr>"
                        +"<tr><td>content</td><td>"+content+"</td></tr>"
                        +"<tr><td>note</td><td>"+note+"</td></tr>"
                        +"<tr><td>time</td><td>"+time+"</td></tr>"
                        +"<tr><td>source</td><td>"+source+"</td></tr>"
                        +"</table>"
                    },
                    error: function(err, x, req) { // on error..
                        //console.log('my message ' + err + " "+ req); 
                    }
        });
    }

    function addTextToSVG(data) {
        addTextToSVGwithDelay(data, 0, data.split("\n").length);
    }

    function addTextToSVGwithDelay(data, cline, maxLength) {
        setTimeout(function() {
            addText(0, 0, currentLine);
            line_start_x = start_x;
            line_start_y = start_y;
            for ( j = 0; j < data.split(/\r?\n/)[cline].split(/(\s+)/).length; j++) {
                if (data.split(/\r?\n/)[cline].split(/(\s+)/)[j] != "") {
                    addTspan(data.split(/\r?\n/)[cline].split(/(\s+)/)[j], line_start_x, line_start_y + currentLine * lineSpacing, data_id, currentLine);
                }
            }
            currentLine++;
            data_id = data_id + 1;
            if (cline < maxLength - 1) {
                addTextToSVGwithDelay(data, cline + 1, maxLength);
            } else if (cline == maxLength - 1) {
                lineCount = currentLine;
                document.getElementById("loading").style.display = "none";
                setSVGHeight();
                fileloadingFinish = true;
            }
        }, 1);
    }

    function addMouseListener() {
        var selStart;
        var selEnd;

        var isClicking = false;
        var isMoving = false;
        var tagging = false;

        $('#svg')[0].addEventListener("mousedown", function() {
            isClicking = true;
        }, false);

        $('#svg')[0].addEventListener("mouseup", function() {
            isClicking = false;
            document.getElementById("delButton").style.display = "none";
            var sel = getSelectionCharOffsetsWithin(document.getElementById("svg"));
            if (isMoving == true) {
                isMoving = false;
                cleanSelection();
                selectTag(sel.start, sel.end);
            }
        });

        $("#svg")[0].addEventListener("mousemove", function() {
            if (isClicking == false){
                return;
            } else {
                isMoving = true;
            }
        },false);

        $("#delButton")[0].addEventListener("mouseover", function() {
            document.getElementById("delButtonCircle").style.fill = "rgb(162,18,18)";
            document.getElementById("delButtonText").style.fill = "white";
        }, false);

        $("#delButton")[0].addEventListener("mouseout", function() {
            document.getElementById("delButtonCircle").style.fill = "red";
            document.getElementById("delButtonText").style.fill = "white";
            //console.log("218");
        }, false);
        /*
         $("#delButton")[0].addEventListener("mouseout", function() {
         document.getElementById("delButton").style.display = "block";
         document.getElementById("delButtonHover").style.display = "none";
         }, false);*/
        $("#delButton")[0].addEventListener("click", function() {
            tagging = true;
            deleteTag(document.getElementById("delButton").getAttribute("tagid"));
            var currentY = document.getElementById("delButton").getAttribute("tagY");
            var line = document.getElementById("delButton").getAttribute("line");
            var tagLine = document.getElementById("delButton").getAttribute("tagLine");
            //console.log("372372372" + getTagLineHasWord(line, tagLine) + getLineHasWord(line, tagLine));
            //console.log(tagLine + " " + line + "373");
            if (getLineHasWord(line, tagLine) == true || getTagLineHasWord(line, tagLine) == true) {
                //console.log("276");
                changeTextY((parseInt)(currentY), "down");
                changeHighLightY((parseInt)(currentY), "down");
                changeTagY((parseInt)(currentY), "down");
                changeTagRectY((parseInt)(currentY) , "down");
            }
        }, false);
    }

    function deleteTag(tagid) {
        //find attribute
        //delete tag attr in db
        for(var currentTextCount = 0 ; currentTextCount <  document.getElementsByClassName("highlight")[0].childNodes.length; currentTextCount++) {
            if (document.getElementsByClassName("highlight")[0].childNodes[currentTextCount].getAttribute("tagid") == tagid) {
                var node = document.getElementsByClassName("highlight")[0].childNodes[currentTextCount]
                var offset_start = node.getAttribute("offset_start");
                var offset_end = node.getAttribute("offset_end");
                var tag_type = node.getAttribute("tagText");
                var source = node.getAttribute("source");
                deleteTagInDatabase(offset_start , offset_end , tag_type, source);
                break;
                //currentTextCount= 0;
            }
        }

        for (var currentTextCount = 0; currentTextCount < document.getElementsByClassName("highlight")[0].childNodes.length; currentTextCount++) {
            if (document.getElementsByClassName("highlight")[0].childNodes[currentTextCount].getAttribute("tagid") == tagid) {
                document.getElementsByClassName("highlight")[0].removeChild(document.getElementsByClassName("highlight")[0].childNodes[currentTextCount]);
                currentTextCount = 0;
            }
        }

        for (var currentTextCount = 0; currentTextCount < document.getElementsByClassName("tag")[0].childNodes.length; currentTextCount++) {
            if (document.getElementsByClassName("tag")[0].childNodes[currentTextCount].getAttribute("tagid") == tagid) {
                document.getElementsByClassName("tag")[0].removeChild(document.getElementsByClassName("tag")[0].childNodes[currentTextCount]);
                currentTextCount = 0;
            }
            //console.log(currentTextCount);
        }

        for (var currentTextCount = 0; currentTextCount < document.getElementsByClassName("tagRect")[0].childNodes.length; currentTextCount++) {
            if (document.getElementsByClassName("tagRect")[0].childNodes[currentTextCount].getAttribute("tagid") == tagid) {
                document.getElementsByClassName("tagRect")[0].removeChild(document.getElementsByClassName("tagRect")[0].childNodes[currentTextCount]);
                currentTextCount = 0;
            }
            //console.log(currentTextCount);
        }

        document.getElementById("delButton").style.display = "none";
    }

    function deleteTagInDatabase(offset_start, offset_end, tag_type, source){
        //offset is 9
        $.ajax({
            type: "POST",      
            data:  { csrfmiddlewaretoken: getCookie('csrftoken'),
                    action : 'deleteTag',
                    file_id:  localStorage.getItem('file_id'),
                    offset_start:offset_start,
                    offset_end: offset_end,
                    tag_type:tag_type,
                    source:  source,
                    },
                    success: function(response) { // on success..
                        //console.log("Delete successfully"); 
                    },
                    error: function(err, x, req) { // on error..
                        //console.log('my message ' + err + " "+ req); 
                    }
        });
    }

    //html5 local storage
    function selectTag(start, end) {
        if(end-start !=0){
            //console.log(start +" " + end);
            //store information in storage
            if ( typeof (Storage) != "undefined") {
                // Store
                localStorage.setItem("offset_start", start);
                localStorage.setItem("offset_end" , end);
                localStorage.setItem("isEdit" , false);
                localStorage.setItem("content" , txt.replace(/\r?\n/g,"").replace(/ /g,"").substring(start-9,end-9));
            } else {
                //error
            }
            
            dialog = new BootstrapDialog({
                message: function(dialog) {
                        var $message = $('<div></div>');
                        //default
                        var pageToLoad = dialog.getData('pageToLoad');
                        $message.load(pageToLoad);
            
                        return $message;
                },
                data: {
                    'pageToLoad': 'tag'
                }
                   
            });
            dialog.realize();
            dialog.getModalHeader().hide();
            dialog.open();
        }
    }

    //TODO: start and end need to be edit
    function highlightText(start, end, color, tagText , source) {
        var currentPosition = 1;
        var offset = 9;
        var linespace = 5;
        var color = color;
        var stop = false;
        start = start - offset + 1;
        end = end - offset + 1;
        var range_start = start;
        var tagid = makeTagId(10);

        for (var currentline = 0; currentline < lineCount && !stop; currentline++) {
            for (var currentTextCount = 1; currentTextCount < document.getElementById("line" + currentline).childNodes.length; currentTextCount++) {
                //console.log(document.getElementById("line" + currentline).childNodes[currentTextCount]);
                if (currentPosition + document.getElementById("line" + currentline).childNodes[currentTextCount].firstChild.length <= start) {
                    currentPosition += document.getElementById("line" + currentline).childNodes[currentTextCount].firstChild.length;
                    // console.log("116");
                } else {
                    var start_distance = start - currentPosition;
                    //var annotatition_length = document.getElementById("line" + currentline).childNodes[currentTextCount].firstChild.length
                    var thisTextEnd;
                    if (currentPosition + document.getElementById("line" + currentline).childNodes[currentTextCount].firstChild.length >= end) {
                        thisTextEnd = end;
                    } else {
                        thisTextEnd = currentPosition + document.getElementById("line" + currentline).childNodes[currentTextCount].firstChild.length;
                    }
                    //console.log(thisTextEnd + "   134" + currentPosition);
                    //currentPosition = start
                    //var x = document.getElementById("line" + currentline).childNodes[currentTextCount].getAttribute("x");
                    //console.log(document.getElementById("line" + currentline).childNodes[currentTextCount].getStartPositionOfChar(3));
                    //console.
                    // console.log(document.getElementById("line" + currentline).childNodes[currentTextCount].getExtentOfChar(3));
                    //console.log("136" + start - 1 + " " + currentTextCount);
                    // console.log( document.getElementById("line" + currentline).childNodes[currentTextCount].getStartPositionOfChar(3).x);
                    var x = document.getElementById("line" + currentline).childNodes[currentTextCount].getExtentOfChar(start - currentPosition).x;
                    //10 is height
                    var y = document.getElementById("line" + currentline).childNodes[currentTextCount].getAttribute("y") - 10;
                    // if(thisTextEnd -  )
                    var width;
                    //console.log(thisTextEnd + " " + start + " " + document.getElementById("line" + currentline).childNodes[currentTextCount].firstChild.length);
                    //console.log(end + " " + " " + (document.getElementById("line" + currentline).childNodes[currentTextCount].firstChild.length + currentPosition) + "S");
                    if (thisTextEnd == document.getElementById("line" + currentline).childNodes[currentTextCount].firstChild.length + start && thisTextEnd - start == document.getElementById("line" + currentline).childNodes[currentTextCount].firstChild.length) {
                        width = document.getElementById("line" + currentline).childNodes[currentTextCount].getComputedTextLength();
                        //console.log("140");
                        /*
                         * abc
                         * 1234
                         * [2,3]
                         * start = 2
                         * currentPosition = 1
                         */
                    } else if (thisTextEnd == document.getElementById("line" + currentline).childNodes[currentTextCount].firstChild.length + currentPosition && thisTextEnd - start != document.getElementById("line" + currentline).childNodes[currentTextCount].firstChild.length) {
                        width = document.getElementById("line" + currentline).childNodes[currentTextCount].getComputedTextLength() - document.getElementById("line" + currentline).childNodes[currentTextCount].getExtentOfChar(start - currentPosition).x + document.getElementById("line" + currentline).childNodes[currentTextCount].getExtentOfChar(0).x;
                        //console.log("143" + " " + document.getElementById("line" + currentline).childNodes[currentTextCount].getExtentOfChar(start - currentPosition).x + " " + document.getElementById("line" + currentline).childNodes[currentTextCount].getComputedTextLength());
                    } else {
                        width = document.getElementById("line" + currentline).childNodes[currentTextCount].getExtentOfChar(thisTextEnd - currentPosition).x - document.getElementById("line" + currentline).childNodes[currentTextCount].getExtentOfChar(start - currentPosition).x;
                        //console.log("143" + " " + document.getElementById("line" + currentline).childNodes[currentTextCount].getExtentOfChar(start - 1).x + " " +document.getElementById("line" + currentline).childNodes[currentTextCount].getExtentOfChar(thisTextEnd - 1).x ) ;
                        //console.log("152");
                    }
                    //start from 1(not 0)
                    //var range = range_start + "," + end;
                    //try to start from 0.
                    //var range = (range_start - 1) + "," + (end - 1);

                    addRect(x, y, width, color, range_start-1 , end-1, currentline, tagText, tagid , source);
                    //console.log(x + " " + y + " " + width + " " + color + " " + range);
                    currentPosition += document.getElementById("line" + currentline).childNodes[currentTextCount].firstChild.length;
                    start = currentPosition;
                    //console.log("149" + currentPosition);
                    if (currentPosition >= end) {
                        //console.log("119");
                        stop = true;
                        break;
                    }
                }
            }
        }
        setSVGHeight();
    }

    //add rect then add tag
    function addRect(x, y, width, color, offset_start , offset_end, currentline, tagText ,tagid , source) {
        //console.log("428  " + color);
        var newText = document.createElementNS("http://www.w3.org/2000/svg", "rect");
        newText.setAttributeNS(null, "x", x);
        newText.setAttributeNS(null, "y", y);
        //newText.setAttributeNS(null, "highlight-range", range);
        newText.setAttributeNS(null, "offset_start" , offset_start);
        newText.setAttributeNS(null, "offset_end" , offset_end);
        newText.setAttributeNS(null, "fill", color);
        newText.setAttributeNS(null, "fill-opacity", 0.5);
        newText.setAttributeNS(null, "width", width);
        newText.setAttributeNS(null, "height", 10);
        newText.setAttributeNS(null, "currentLine", currentline);
        newText.setAttributeNS(null, "tagText" , tagText);
        newText.setAttributeNS(null, "tagid", tagid);
        newText.setAttributeNS(null, "source" , source);

        var textNode = document.createTextNode("");
        newText.appendChild(textNode);
        document.getElementsByClassName("highlight")[0].appendChild(newText);
        addTag(x + width / 2, y, currentline, tagText, tagid , source);
    }

    function addText(x, y, lineNumber) {
        var newText = document.createElementNS("http://www.w3.org/2000/svg", "text");
        newText.setAttributeNS(null, "x", x);
        newText.setAttributeNS(null, "y", y);
        newText.setAttributeNS(null, "id", "line" + lineNumber);
        var textNode = document.createTextNode("");
        newText.appendChild(textNode);
        document.getElementsByClassName("text")[0].appendChild(newText);
    }

    function addTspan(text, x, y, id, lineNumber) {
        var newText = document.createElementNS("http://www.w3.org/2000/svg", "tspan");
        newText.setAttributeNS(null, "data-id", id);
        newText.setAttributeNS(null, "x", x);
        newText.setAttributeNS(null, "y", y);
        var textNode = document.createTextNode(text);
        newText.appendChild(textNode);
        //console.log("268 newText.getComputedTextLength()" +newText.getComputedTextLength());
        document.getElementById(("line" + lineNumber)).appendChild(newText);
        // console.log("274 linenumber "+ lineNumber + " line_start_x " + line_start_x);
        line_start_x = line_start_x + newText.getComputedTextLength() + wordSpacingLength;
        //console.log("282 " + text + "   " + newText.getComputedTextLength());
        //console.log("283 line_start_x " + line_start_x);

        if (line_start_x - wordSpacingLength > maxXPerLine) {
            //
            var hasFinish = true;
            var leftLength = maxXPerLine - (line_start_x - newText.getComputedTextLength() - wordSpacingLength);
            line_start_x -= newText.getComputedTextLength() + wordSpacingLength;
            //console.log("237  " + leftLength);
            var sliceIndex;
            for ( i = 1; i <= text.length; i++) {
                var tempnewText = document.createElementNS("http://www.w3.org/2000/svg", "tspan");
                tempnewText.setAttributeNS(null, "data-id", id);
                tempnewText.setAttributeNS(null, "x", x + leftLength);
                tempnewText.setAttributeNS(null, "y", y);
                var textNode = document.createTextNode(text.substring(0, i));
                tempnewText.appendChild(textNode);
                //console.log(tempnewText + " " + text.substring(0, i) + "245");
                //console.log(lineNumber + " 246 " + i);
                //console.log(document.getElementById(("line" + lineNumber)) + "   " + lineNumber + " 247");
                document.getElementById(("line" + lineNumber)).appendChild(tempnewText);
                //console.log("303 " + leftLength + " " + tempnewText.getComputedTextLength() + " text sub " + text.substring(0, i) + " " + i + " " + text.length);
                if (leftLength - tempnewText.getComputedTextLength() >= 0) {
                    document.getElementById(("line" + lineNumber)).removeChild(tempnewText);
                } else {
                    sliceIndex = i - 1;
                    //console.log("252   " + sliceIndex + "  Line " + currentLine + " " + text.substring(0, sliceIndex));
                    document.getElementById(("line" + lineNumber)).removeChild(tempnewText);
                    //currentLine +=1;
                    addTspan(text.substring(0, sliceIndex), line_start_x, line_start_y + currentLine * lineSpacing, id, currentLine);
                    hasFinish = false;
                    break;
                }

            }
            if (!hasFinish) {
                document.getElementById(("line" + lineNumber)).removeChild(newText);
                line_start_x = start_x;
                currentLine += 1;
                addText(0, 0, currentLine);
                //console.log("292" + text.substring(sliceIndex, text.length) + "   " + leftLength + "  " + text.length);
                addTspan(text.substring(sliceIndex, text.length), line_start_x, line_start_y + currentLine * lineSpacing, id, currentLine);
            }
        }
    }

    function addTag(x, y, currentline, tag, tagid, source) {
        var highlightAndTextHeightDelta = 10;
        var width;
        var tagLine = 0;
        var needNewLine = true;
        var newTag = document.createElementNS("http://www.w3.org/2000/svg", "text");
        newTag.setAttributeNS(null, "x", x);

        //10 stands for the height of x;
        newTag.setAttributeNS(null, "y", y + highlightAndTextHeightDelta);

        var textNode = document.createTextNode(tag);
        newTag.appendChild(textNode);
        document.getElementsByClassName("tag")[0].appendChild(newTag);
        newTag.setAttributeNS(null, "line", currentline);
        width = newTag.getComputedTextLength();
        x -= newTag.getComputedTextLength() / 2;
        //put the texts in the middle
        if (x-leftOffset <= 0) {
            x = leftOffset;
            while (tagDetect(x, width, currentline, tagLine) == true) {
                tagLine += 1;
            }
        } else if( x + width>= maxXPerLine) {
            x = maxXPerLine - width;
            while (tagDetect(x, width, currentline, tagLine) == true) {
                tagLine += 1;
            }

        } else{
            while (tagDetect(x, width, currentline, tagLine) == true) {
                tagLine += 1;
            }     
        }

        document.getElementsByClassName("tag")[0].removeChild(newTag);

        for (var currentTextCount = 0; currentTextCount < document.getElementsByClassName("tag")[0].childNodes.length; currentTextCount++) {
            if (document.getElementsByClassName("tag")[0].childNodes[currentTextCount].getAttribute("line") == currentline) {
                //y = (parseInt)(document.getElementsByClassName("tag")[0].childNodes[currentTextCount].getAttribute("y"));
                if (document.getElementsByClassName("tag")[0].childNodes[currentTextCount].getAttribute("tagLine") == tagLine) {
                    needNewLine = false;
                    break;
                }
            }
        }
        //console.log("364 " + tagLine);
        //console.log("365 " + needNewLine + " " + y);
        //add again;
        var newTag = document.createElementNS("http://www.w3.org/2000/svg", "text");
        if (needNewLine) {
            y = y + highlightAndTextHeightDelta - tagLine * lineSpacing;
            changeTagY(y, "up");
            changeTagRectY(y-14,"up");
        } else {
            y = (parseInt)(document.getElementsByClassName("tag")[0].childNodes[currentTextCount].getAttribute("y"));
        }
        //console.log("595" + y);
        newTag.setAttributeNS(null, "x", x);
        //hightlightAndTextHeightDelta stands for the height of x;
        newTag.setAttributeNS(null, "y", y);
        newTag.setAttributeNS(null, "line", currentline);
        newTag.setAttributeNS(null, "width", width);
        newTag.setAttributeNS(null, "tagLine", tagLine);
        newTag.setAttributeNS(null, "tagid", tagid);
        newTag.setAttributeNS(null, "font-size" , 19);
        newTag.setAttributeNS(null, "source" , source);
        //console.log(y);
        var textNode = document.createTextNode(tag);
        newTag.appendChild(textNode);
        document.getElementsByClassName("tag")[0].appendChild(newTag);
        //newTag.addEventListener('mouseover', tagmouseoverEvent, false);
        //newTag.addEventListener('mouseout', tagmouseoutEvent, false);
        //newTag.addEventListener('click' , tagmouseclickEvent , false);

        //new tag rect 
        var newTagRect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
        newTagRect.setAttributeNS(null, "x", x-leftOffset);
        newTagRect.setAttributeNS(null, "y", y-14);
        newTagRect.setAttributeNS(null, "fill", 'blue');
        newTagRect.setAttributeNS(null, "fill-opacity", 0.1);
        newTagRect.setAttributeNS(null, "width", width-3);
        newTagRect.setAttributeNS(null, "stroke-width" ,2);
        newTagRect.setAttributeNS(null, "stroke" , "rgb(0,0,0)");
        newTagRect.setAttributeNS(null, "height", 20);
        newTagRect.setAttributeNS(null, "line",currentline);
        newTagRect.setAttributeNS(null, "tagLine", tagLine);
        newTagRect.setAttributeNS(null, "tagid", tagid);
        newTagRect.setAttributeNS(null, "source" , source);
        //console.log(y);
        var textNode = document.createTextNode("");
        newTagRect.appendChild(textNode);
        //console.log(newTagRect);
        document.getElementsByClassName("tagRect")[0].appendChild(newTagRect);
        newTagRect.addEventListener('mouseover', tagmouseoverEvent, false);
        newTagRect.addEventListener('mouseout', tagmouseoutEvent, false);
        newTagRect.addEventListener('click' , tagmouseclickEvent , false);
        newTagRect.addEventListener('dblclick' , tagmousedbclickEvent, false);
        //add new line
        if (needNewLine) {
            changeTextY(y, "up");
            changeHighLightY(y, "up");
        }  
    }
    
    function tagDetect(x, width, line, tagLine) {
        for (var currentTextCount = 0; currentTextCount < document.getElementsByClassName("tag")[0].childNodes.length; currentTextCount++) {
            if (document.getElementsByClassName("tag")[0].childNodes[currentTextCount].getAttribute("line") == line && document.getElementsByClassName("tag")[0].childNodes[currentTextCount].getAttribute("tagLine") == tagLine) {
                var element = document.getElementsByClassName("tag")[0].childNodes[currentTextCount];
                var elementWidth = (parseFloat)(element.getAttribute("width"));
                var elementX = (parseFloat)(element.getAttribute("x"));
                //console.log(x + "x " + width + " width" + elementX + "elementX " + elementWidth);
                //console.log(x-leftOffset + width >= elementX && x-leftOffset + width <= elementX + elementWidth);
                //console.log( x >= elementX && x <= elementX + elementWidth + " " + x >= elementX + " " +x <= elementX + elementWidth );
                if ((x + width >= elementX && x + width <= elementX + elementWidth ) || (x >= elementX && x <= elementX + elementWidth) || x <=elementX && x+width >= elementX + elementWidth) {
                    //console.log("393");
                    return true;
                }
            }
        }
        return false;
    }

    function changeTextY(y, dir) {
        //change text's y
        if (dir == "up") {
            for ( lN = 0; lN < lineCount; lN++) {
                //index start from 1
                for (var currentTextCount = 1; currentTextCount < document.getElementById("line" + lN).childNodes.length; currentTextCount++) {
                    //console.log(document.getElementById("line" + lN).childNodes[1]);
                    var lineY = document.getElementById("line" + lN).childNodes[currentTextCount].getAttribute("y");
                    if (lineY >= y) {
                        //console.log(lineY + " " + "AAA" + currentTextCount + " " + lN);
                        lineY = (parseInt)(lineY) + (parseInt)(lineSpacing);
                        //console.log("line" + lineY);
                        document.getElementById("line" + lN).childNodes[currentTextCount].setAttribute("y", lineY);
                    }
                }
            }
        } else if (dir == "down") {
            for ( lN = 0; lN < lineCount; lN++) {
                //index start from 1
                for (var currentTextCount = 1; currentTextCount < document.getElementById("line" + lN).childNodes.length; currentTextCount++) {
                    //console.log(document.getElementById("line" + lN).childNodes[1]);
                    var lineY = document.getElementById("line" + lN).childNodes[currentTextCount].getAttribute("y");
                    if (lineY >= y) {
                        //console.log(lineY + " " + "AAA" + currentTextCount + " " + lN);
                        //console.log(lineY + " " + y);
                        lineY = (parseInt)(lineY) - (parseInt)(lineSpacing);
                        //console.log("line" + lineY);
                        document.getElementById("line" + lN).childNodes[currentTextCount].setAttribute("y", lineY);
                    }
                }
            }
        }
    }


    function changeTagY(y, dir) {
        //console.log("470 " + y);
        var tagLength = document.getElementsByClassName("tag")[0].childNodes.length;
        if (dir == "up") {
            for (var currentTagCount = 0; currentTagCount < tagLength; currentTagCount++) {
                var elementY = document.getElementsByClassName("tag")[0].childNodes[currentTagCount].getAttribute("y");
                //console.log(elementY + " " + y);
                if (elementY >= y) {
                    elementY = (parseInt)(elementY) + (parseInt)(lineSpacing);
                    document.getElementsByClassName("tag")[0].childNodes[currentTagCount].setAttribute("y", elementY);
                }
                //console.log(elementY);
            }
        } else if (dir == "down") {
            for (var currentTagCount = 0; currentTagCount < tagLength; currentTagCount++) {
                var elementY = document.getElementsByClassName("tag")[0].childNodes[currentTagCount].getAttribute("y");
                //console.log(elementY + " " + y);
                if (elementY >= y) {
                    elementY = (parseInt)(elementY) - (parseInt)(lineSpacing);
                    document.getElementsByClassName("tag")[0].childNodes[currentTagCount].setAttribute("y", elementY);
                }
                //console.log(elementY);
            }
        }

    }

    function changeTagRectY(y,dir){
        var tagRectLength = document.getElementsByClassName("tagRect")[0].childNodes.length;
        if (dir == "up") {
            for (var currentTagCount = 0; currentTagCount < tagRectLength; currentTagCount++) {
                var elementY = document.getElementsByClassName("tagRect")[0].childNodes[currentTagCount].getAttribute("y");
                //console.log(elementY + " " + y);
                if (elementY >= y) {
                    elementY = (parseInt)(elementY) + (parseInt)(lineSpacing);
                    document.getElementsByClassName("tagRect")[0].childNodes[currentTagCount].setAttribute("y", elementY);
                }
            }
        } else if (dir == "down") {
            for (var currentTagCount = 0; currentTagCount < tagRectLength; currentTagCount++) {
                var elementY = document.getElementsByClassName("tagRect")[0].childNodes[currentTagCount].getAttribute("y");
                //console.log(elementY + " " + y);
                if (elementY >= y) {
                    elementY = (parseInt)(elementY) - (parseInt)(lineSpacing);
                    document.getElementsByClassName("tagRect")[0].childNodes[currentTagCount].setAttribute("y", elementY);
                }
            }
        }
    }

    //up and down
    function changeHighLightY(y, dir) {
        var highlightAndTextHeightDelta = 10;
        var highlightLength = document.getElementsByClassName("highlight")[0].childNodes.length;
        if (dir == "up") {
            for (var currentTagCount = 0; currentTagCount < highlightLength; currentTagCount++) {
                var elementY = (parseInt)(document.getElementsByClassName("highlight")[0].childNodes[currentTagCount].getAttribute("y")) + (parseInt)(highlightAndTextHeightDelta);
                //console.log("416 " + elementY + " " + y);
                if (elementY >= y) {
                    elementY = (parseInt)(elementY) + (parseInt)(lineSpacing);
                    //console.log("434 " + elementY);
                    document.getElementsByClassName("highlight")[0].childNodes[currentTagCount].setAttribute("y", elementY - highlightAndTextHeightDelta);
                }
            }
        } else if (dir == "down") {
            for (var currentTagCount = 0; currentTagCount < highlightLength; currentTagCount++) {
                var elementY = (parseInt)(document.getElementsByClassName("highlight")[0].childNodes[currentTagCount].getAttribute("y")) + (parseInt)(highlightAndTextHeightDelta);
                //console.log("416 " + elementY + " " + y);
                if (elementY >= y) {
                    elementY = (parseInt)(elementY) - (parseInt)(lineSpacing);
                    document.getElementsByClassName("highlight")[0].childNodes[currentTagCount].setAttribute("y", elementY - highlightAndTextHeightDelta);
                }
            }
        }
    }

    function getTagLineHasWord(curLine, curTagLine) {
        var needDelete = true;
        var tagLength = document.getElementsByClassName("tag")[0].childNodes.length;
        for (var currentTagCount = 0; currentTagCount < tagLength; currentTagCount++) {
            var line = document.getElementsByClassName("tag")[0].childNodes[currentTagCount].getAttribute("line");
            var tagLine = document.getElementsByClassName("tag")[0].childNodes[currentTagCount].getAttribute("tagLine");
            if (curLine == line && curTagLine == tagLine) {
                needDelete = false;
                break;
            }
        }
        return needDelete;
    }

    function getLineHasWord(curLine, curTagLine) {
        var needDelete = true;
        var tagLength = document.getElementsByClassName("tag")[0].childNodes.length;
        for (var currentTagCount = 0; currentTagCount < tagLength; currentTagCount++) {
            var line = document.getElementsByClassName("tag")[0].childNodes[currentTagCount].getAttribute("line");
            if (curLine == line) {
                needDelete = false;
                break;
            }
        }
        return needDelete;
    }

    function getSelectionCharOffsetsWithin(element) {
        var start = 0,
            end = 0;
        var sel,
            range,
            priorRange;
        if ( typeof window.getSelection != "undefined") {
            range = window.getSelection().getRangeAt(0);
            priorRange = range.cloneRange();
            priorRange.selectNodeContents(element);
            priorRange.setEnd(range.startContainer, range.startOffset);
            start = priorRange.toString().length;
            end = start + range.toString().length;
        } else if ( typeof document.selection != "undefined" && ( sel = document.selection).type != "Control") {
            range = sel.createRange();
            priorRange = document.body.createTextRange();
            priorRange.moveToElementText(element);
            priorRange.setEndPoint("EndToStart", range);
            start = priorRange.text.length;
            end = start + range.text.length;
        }
        return {
            start : start,
            end : end
        };
    }

    function cleanSelection() {
        if (window.getSelection) {
            if (window.getSelection().empty) {// Chrome
                window.getSelection().empty();
            } else if (window.getSelection().removeAllRanges) {// Firefox
                window.getSelection().removeAllRanges();
            }
        } else if (document.selection) {// IE?
            document.selection.empty();
        }
    }

    function makeTagId(length) {

        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < length; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;

    }

    function getCookie(name) {
        var cookieValue = null;
        //console.log("")
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            //console.log(cookies);
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

})();
