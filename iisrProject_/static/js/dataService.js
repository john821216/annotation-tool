var Datas = (function() {
    var hasPressAddFileDataBtn = false;
    var hasPressAddTagDataBtn = false;
    var hasPressAddTagInfoBtn = false;
    var newRow;
    var table;
    return{
        addFileData : addFileData,
        addTagData: addTagData,
        addTagInfoData : addTagInfoData,
        exportTxt : exportTxt,
        initFile : initFile,
        initTag : initTag,
        initTagInfo: initTagInfo,
        initSideBarIndex : initSideBarIndex,
        initTxtUpload : initTxtUpload,
        initTagInfoUpload: initTagInfoUpload,
        deleteFileIdAndSource : deleteFileIdAndSource,
    };

    function initFile(){
        var editFileElem = document.getElementsByName("action");
        //console.log(editFileElem.length);

        //edit and remove
        for(var i = 0 ; i < editFileElem . length ; i++){
            if(editFileElem[i].getAttribute("value") == 'edit'){
                editFileElem[i].addEventListener("click", editFileRow ,false);
                editFileElem[i].i = i/2;
            }
            else if(editFileElem[i].getAttribute("value") == 'remove'){
                editFileElem[i].addEventListener("click" ,deleteFileRow , false);
                editFileElem[i].i = (i-1)/2;
            }
        }
    }

    function initSideBarIndex(){
        $(document).on('change', '#indexUpload :file', function() {
            var input = $(this),
                numFiles = input.get(0).files ? input.get(0).files.length : 1,
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [numFiles, label]);
        });

        $('#indexUpload :file').on('fileselect', function(event, numFiles, label) {
            //console.log(numFiles);
            //console.log(label);
            $('#indexUploadForm').submit();
            alert('Upload Index File Successfully');
        });
    }

    function initTxtUpload(){
        $(document).on('change', '#txtUpload :file', function() {
            var input = $(this),
                numFiles = input.get(0).files ? input.get(0).files.length : 1,
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [numFiles, label]);
        });

        $('#txtUpload :file').on('fileselect', function(event, numFiles, label) {
            //console.log(numFiles);
            //console.log(label);
            $('#txtUploadForm').submit();
            alert('Upload Txts Successfully');
        });
    }

    function initTagInfoUpload(){
        $(document).on('change', '#tagInfoUpload :file', function() {
            var input = $(this),
                numFiles = input.get(0).files ? input.get(0).files.length : 1,
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [numFiles, label]);
        });

        $('#tagInfoUpload :file').on('fileselect', function(event, numFiles, label) {
            //console.log(numFiles);
            //console.log(label);
            $('#tagInfoForm').submit();
            alert('Upload TagsInfo Successfully');
        });
    }

    function deleteFileIdAndSource(hasConfirmLength){
        var file_id = document.getElementById('delFileData').textContent || document.getElementById('delFileData').innerHTML ||document.getElementById('delFileData').value ;
        var source = document.getElementById('delSourceData').textContent || document.getElementById('delSourceData').innerHTML || document.getElementById('delSourceData').value;
        //console.log(file_id + " " + source);
        if(!hasConfirmLength){
            $.ajax({
                    type: "POST",       
                    data: { csrfmiddlewaretoken: getCookie('csrftoken'),
                            action : 'deleteFileIdAndSourceDataLength',
                            file_id: file_id,
                            source : source,      
                          },
                    success:function(data, textStatus) {
                        if(data.length > 0){
                           BootstrapDialog.confirm({
                                title: 'WARNING',
                                message: 'Delete ' +data.length+ ' datas, Are you sure?',
                                callback: function(result) {
                                // result will be true if button was click, while it will be false if users close the dialog directly.
                                    if(result) {
                                        deleteFileIdAndSource(true);
                                    }
                                }
                            });
                        } else {
                            BootstrapDialog.alert({
                                title: 'WARNING',
                                type: BootstrapDialog.TYPE_DANGER,
                                message: 'NO SUCH DATA',
                            });
                        }
                    },
                    error: function(req, err){
                     //console.log('my message ' + err + " "+ req); 
                    }
            });
        } else {
            $.ajax({
                    type: "POST",       
                    data: { csrfmiddlewaretoken: getCookie('csrftoken'),
                            action : 'deleteFileIdAndSource',
                            file_id: file_id,
                            source : source,      
                          },
                    success:function(data, textStatus) {
                        window.location.reload();
                    },

                    error: function(req, err){
                        //console.log('my message ' + err + " "+ req); 
                    }
            });
        }
    }


    function addTagData(){
         if(!hasPressAddTagDataBtn){
            table =document.getElementById("table");
            //console.log(table);
            newRow = table.insertRow(1);
            var name = newRow.insertCell(0);
            var color = newRow.insertCell(1);
            var confirmAndCancel = newRow.insertCell(2);
            confirmAndCancel.align = "right"; 
            name.innerHTML = "<input type = 'text' name='name' maxlength='50' id='name' required/>" ;
            color.innerHTML = "<input type = 'text' name='parent'  maxlength='25' id ='color' required/>";
            confirmAndCancel.innerHTML= "<button type= 'submit' class='btn btn-default' aria-label='Left Align' id='submit' name ='action' value='add'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button><button type='button' class='btn btn-default' aria-label='Left Align'><span class='glyphicon glyphicon-remove' aria-hidden='true' id='removeUnsaveBtn'></span></button>";
            hasPressAddTagDataBtn = true; 
            document.getElementById("removeUnsaveBtn").addEventListener("mousedown", removeUnsaveNewTagRow,false);
        }
    }

    function addFileData(){
        if(!hasPressAddFileDataBtn){
            table =document.getElementById("table");
            //console.log(table);
            newRow = table.insertRow(1);
            var name = newRow.insertCell(0);
            var parent = newRow.insertCell(1);
            var type = newRow.insertCell(2);
            var fileName = newRow.insertCell(3);
            var tagFileName = newRow.insertCell(4);
            var confirmAndCancel = newRow.insertCell(5);
            confirmAndCancel.align = "right";
            name.innerHTML = "<input type = 'text' name='name' maxlength='10' id='name' required/>" ;
            parent.innerHTML = "<input type = 'text' name='parent'  maxlength='10' id ='parent' required/>";
            type.innerHTML = "<input type = 'text' name='type'  maxlength='10' id = 'type' required />";
            fileName.innerHTML = "<input type = 'text' name='fileName'  maxlength='10' id ='fileName' required />";
            tagFileName.innerHTML = "<input type = 'text' name='tagFileName'  maxlength='10' id= 'tagFileName' required/>";
            confirmAndCancel.innerHTML= "<button type= 'submit' class='btn btn-default' aria-label='Left Align' id='submit' name ='action' value='add'><span class='glyphicon glyphicon-ok' aria-hidden='true' id ='addNewDataBtn'></span></button><button type='button' class='btn btn-default' aria-label='Left Align'><span class='glyphicon glyphicon-remove' aria-hidden='true' id='removeUnsaveBtn'></span></button>";
            hasPressAddFileDataBtn = true; 
            // document.getElementById('addNewDataBtn').addEventListener("mousedown",addDataToFileDB, false);
            //document.getElementById(“removeUnsaveBtn”).addEventListener("mousedown",removeUnsaveNewRow , false);
            document.getElementById("removeUnsaveBtn").addEventListener("mousedown", removeUnsaveNewRow,false);

        }
    }

    function exportTxt(){
        $.ajax({
            type: "POST",       
            data: { csrfmiddlewaretoken: getCookie('csrftoken'),
                    action : 'exportTxtFile',     
                  },
            success:function(data, textStatus) {
               // console.log("Export successfully"); 
               // console.log(data);
               // console.log(textStatus);
            },
            error: function(req, err){
               // console.log('my message ' + err + " "+ req); 
            }
        });
    }

    function editFileRow(event){
      //  console.log(row +"edit");
       // console.log(event.target.i);
        var row = event.target.i;
        var cells =document.getElementById("table").rows[row+1].cells;
        //console.log(editElem);
        nameStr = cells[0].textContent || cells[0].innerHTML;
        parentStr = cells[1].textContent || cells[1].innerHTML;
        f_typeStr = cells[2].textContent || cells[2].innerHTML;
        file_nameStr = cells[3].textContent || cells[3].innerHTML;
        tagFile_nameStr =cells[4].textContent || cells[4].innerHTML;
      //  console.log(nameStr +" " + parentStr + " " + f_typeStr + " " + file_nameStr + " " + tagFile_nameStr);
        cells[0].innerHTML = "<input type = 'text' name='name' maxlength='10' id='name' value= '"+nameStr+"'required/>" ;
        cells[1].innerHTML = "<input type = 'text' name='parent'  maxlength='10' id ='parent' value= '"+parentStr+"' required/>";
        cells[2].innerHTML = "<input type = 'text' name='type'  maxlength='10' id = 'type' value= '"+f_typeStr+"' required />";
        cells[3].innerHTML = "<input type = 'text' name='fileName'  maxlength='10' id ='fileName' value= '"+file_nameStr+"' required />";
        cells[4].innerHTML = "<input type = 'text' name='tagFileName'  maxlength='10' id= 'tagFileName' value= '"+tagFile_nameStr+"' required/>";
        cells[5].innerHTML=  "<button class='btn btn-default' aria-label='Left Align' id='submit' name ='action' value='edit'><span class='glyphicon glyphicon-ok' aria-hidden='true' id ='confirmEditBtn'></span></button><button type='button' class='btn btn-default' aria-label='Left Align'><span class='glyphicon glyphicon-remove' aria-hidden='true' id='cancelEditBtn'></span></button>";
        document.getElementById("cancelEditBtn").addEventListener("mousedown", cancelFileEditBtn,false);
        document.getElementById("cancelEditBtn").row = row;
        document.getElementById("confirmEditBtn").addEventListener("mousedown" , confirmFileEditBtn , false);
    }

    function cancelFileEditBtn(event){
        var row = event.target.row;
        var cells =document.getElementById("table").rows[row+1].cells;
        cells[0].innerHTML =  nameStr;
        cells[1].innerHTML =  parentStr;
        cells[2].innerHTML =  f_typeStr;
        cells[3].innerHTML =  file_nameStr;
        cells[4].innerHTML =  tagFile_nameStr;
        cells[5].innerHTML =  "<button type='button' class='btn btn-default'><span class='glyphicon glyphicon-align-left' aria-hidden='true' name='action' value='edit'></span></button><button type='button' class='btn btn-default'><span class='glyphicon glyphicon-remove' aria-hidden='true' name='action' value='remove'></span></button>";
        window.location.reload();
    }

    function confirmFileEditBtn(){
       // console.log("91");
        $('#f_form').submit(function(e) { // catch the form's submit event
          //  console.log("93");
            e.preventDefault();
            //e.unbind();
            $.ajax({ // create an AJAX call...
                type: "POST",      
                data: $(this).serialize() +"&e_name_on_index=" + nameStr 
                        + "&e_file_parent="+parentStr +"&e_file_type="+f_typeStr
                        + "&e_file_name="+file_nameStr +"&e_tagFile_name="+tagFile_nameStr +"&action=edit",
                success: function(response) { // on success..
                     window.location.reload();
                },
                error: function(e, x, r) { // on error..
                  // console.log('my message ' + err + " "+ req); 
                }
            });
        });
    }

    function deleteFileRow(event){
        var row = event.target.i;
        var cells =document.getElementById("table").rows[row+1].cells;
        name = cells[0].textContent || cells[0].innerHTML;
        parent = cells[1].textContent || cells[1].innerHTML;
        f_type = cells[2].textContent || cells[2].innerHTML;
        file_name = cells[3].textContent || cells[3].innerHTML;
        tagFile_name =cells[4].textContent || cells[4].innerHTML;
        $.ajax({
                type: "POST",       
                data: { csrfmiddlewaretoken: getCookie('csrftoken'),
                        action : 'delete',
                        name_on_index : name , 
                        file_parent : parent,
                        file_type : f_type,
                        file_name : file_name,
                        tagFile_name : tagFile_name,
                        
                      },
                success:function(data, textStatus) {
                    window.location.reload();
                },
                error: function(req, err){
                // console.log('my message ' + err + " "+ req); 
                }
        });
    }

    
    function removeUnsaveNewRow(){
       // console.log("Enter");
        table.deleteRow(1);
        hasPressAddFileDataBtn = false;      
    }

    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    function initTag(){
        var editTagElem = document.getElementsByName("action");
        //console.log(editFileElem.length);

        //edit and remove
        for(var i = 0 ; i < editTagElem . length ; i++){
            if(editTagElem[i].getAttribute("value") == 'edit'){
                editTagElem[i].addEventListener("click", editTagRow ,false);
                editTagElem[i].i = i/2;
            }
            else if(editTagElem[i].getAttribute("value") == 'remove'){
                editTagElem[i].addEventListener("click" ,deleteTagRow , false);
                editTagElem[i].i = (i-1)/2;
            }
        }
    }

    function removeUnsaveNewTagRow(){
       // console.log("Enter");
        table.deleteRow(1);
        hasPressAddTagDataBtn = false;      
    }

    function editTagRow(){
      //  console.log(event.target.i);
        var row = event.target.i;
        var cells =document.getElementById("table").rows[row+1].cells;
        //console.log(editElem);
        nameStr = cells[0].textContent || cells[0].innerHTML;
        colorStr = cells[1].textContent || cells[1].innerHTML;
      //  console.log(nameStr + " " + colorStr);
        cells[0].innerHTML = "<input type = 'text' name='name' maxlength='50' id='name' value= '"+nameStr+"'required/>" ;
        cells[1].innerHTML = "<input type = 'text' name='color'  maxlength='25' id ='color' value= '"+colorStr+"' required/>";
        cells[2].innerHTML=  "<button class='btn btn-default' aria-label='Left Align' id='submit' name ='action' value='edit'><span class='glyphicon glyphicon-ok' aria-hidden='true' id ='confirmEditBtn'></span></button><button type='button' class='btn btn-default' aria-label='Left Align'><span class='glyphicon glyphicon-remove' aria-hidden='true' id='cancelEditBtn'></span></button>";
        document.getElementById("cancelEditBtn").addEventListener("mousedown", cancelTagEditBtn,false);
        document.getElementById("cancelEditBtn").row = row;
        document.getElementById("confirmEditBtn").addEventListener("mousedown" , confirmTagEditBtn , false);
    }

    function confirmTagEditBtn(){
        $('#t_form').submit(function(e) { // catch the form's submit event
            e.preventDefault();
            //e.unbind();
            $.ajax({ // create an AJAX call...
                type: "POST",      
                data: $(this).serialize() +"&e_name=" + nameStr 
                        + "&e_color="+colorStr +"&action=edit",
                success: function(response) { // on success..
                     window.location.reload();
                },
                error: function(e, x, r) { // on error..
                  // console.log('my message ' + err + " "+ req); 
                }
            });
        });
    }

    function deleteTagRow(event){
        var row = event.target.i;
        var cells =document.getElementById("table").rows[row+1].cells;
        name = cells[0].textContent || cells[0].innerHTML;
        color = cells[1].textContent || cells[1].innerHTML;
    
        $.ajax({
                type: "POST",       
                data: { csrfmiddlewaretoken: getCookie('csrftoken'),
                        action : 'delete',
                        tag_name : name , 
                        tag_color : color,
                      },
                success:function(data, textStatus) {
                    window.location.reload();
                },
                error: function(req, err){
                // console.log('my message ' + err + " "+ req); 
                }
        });
    }

    function cancelTagEditBtn(event){
        var row = event.target.row;
        var cells =document.getElementById("table").rows[row+1].cells;
        cells[0].innerHTML =  nameStr;
        cells[1].innerHTML =  colorStr;
        cells[2].innerHTML =  "<button type='button' class='btn btn-default'><span class='glyphicon glyphicon-align-left' aria-hidden='true' name='action' value='edit'></span></button><button type='button' class='btn btn-default'><span class='glyphicon glyphicon-remove' aria-hidden='true' name='action' value='remove'></span></button>";
        window.location.reload();
    }

    function initTagInfo(){
        var editTagInfoElem = document.getElementsByName("action");
       // console.log(editTagInfoElem.length);

        //edit and remove
        for(var i = 0 ; i < editTagInfoElem . length ; i++){
            if(editTagInfoElem[i].getAttribute("value") == 'remove'){
                editTagInfoElem[i].addEventListener("click", deleteTagInfoRow ,false);
                editTagInfoElem[i].i = i;
            }
        }
    }

    function addTagInfoData(){
        if(!hasPressAddTagInfoBtn){
            table =document.getElementById("table");
            newRow = table.insertRow(1);
            var file_id = newRow.insertCell(0);
            var offset_start = newRow.insertCell(1);
            var offset_end= newRow.insertCell(2);
            var tag_type= newRow.insertCell(3);
            var content= newRow.insertCell(4);
            var note= newRow.insertCell(5);
            var time = newRow.insertCell(6);
            var source= newRow.insertCell(7);
            var confirmAndCancel = newRow.insertCell(8);
            confirmAndCancel.align = "right"; 
            file_id.innerHTML = "<input type = 'text' name='file_id' maxlength='30' id='file_id' style='width:100px' required/>" ;
            offset_start.innerHTML = "<input type = 'text' name='offset_start'  maxlength='30' id ='offset_start'  style='width:100px' required/>";
            offset_end.innerHTML="<input type = 'text' name='offset_end' maxlength='30' id='offset_end'  style=='width:100px' required/>" ;
            tag_type.innerHTML="<input type = 'text' name='tag_type' maxlength='30' id='tag_type'  style='width:100px' required/>" ;
            content.innerHTML="<input type = 'text' name='content' maxlength='30' id='content'  style='width:100px' required/>" ;
            note.innerHTML="<input type = 'text' name='note' maxlength='30' id='note'  style='width:100px' required/>" ;
            time.innerHTML="<input type = 'text' name='time' maxlength='30' id='time'  style='width:100px' required/>" ;
            source.innerHTML="<input type = 'text' name='source' maxlength='30' id='source'  style='width:100px' required/>" ;
            confirmAndCancel.innerHTML= "<button type= 'submit' class='btn btn-default' aria-label='Left Align' id='submit' name ='action' value='add'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button><button type='button' class='btn btn-default' aria-label='Left Align'><span class='glyphicon glyphicon-remove' aria-hidden='true' id='removeUnsaveBtn'></span></button>";
            hasPressAddTagDataBtn = true; 
            document.getElementById("removeUnsaveBtn").addEventListener("mousedown", removeUnsaveNewTagRow,false);
        }
    }

    function deleteTagInfoRow(){
        var row = event.target.i;
        var cells =document.getElementById("table").rows[row+1].cells;
        file_id =  cells[0].textContent || cells[0].innerHTML;
        offset_start =  cells[1].textContent || cells[1].innerHTML;
        offset_end=  cells[2].textContent || cells[2].innerHTML;
        tag_type=  cells[3].textContent || cells[3].innerHTML;
        content=  cells[4].textContent || cells[4].innerHTML;
        note=  cells[5].textContent || cells[5].innerHTML;
        time =  cells[6].textContent || cells[6].innerHTML;
        source= cells[7].textContent || cells[7].innerHTML;
        $.ajax({
                type: "POST",       
                data: { csrfmiddlewaretoken: getCookie('csrftoken'),
                        action : 'delete',
                        file_id : file_id , 
                        offset_start : offset_start,
                        offset_end : offset_end,
                        tag_type : tag_type,
                        content : content,
                        note : note,
                        time : time,
                        source : source,
                      },
                success:function(data, textStatus) {
                    window.location.reload();
                },
                error: function(req, err){
               //  console.log('my message ' + err + " "+ req); 
                }
        });
    }
})();