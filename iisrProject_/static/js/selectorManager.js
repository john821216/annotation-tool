var SelectorManager = (function() {
    return{
        ini : function() {
            addSearchListener();
        },
        search : function(fileName,tagTypeArgs,targetSource){
            search(fileName,tagTypeArgs,targetSource);
        },
    };

    function addSearchListener(){
        document.getElementById("searchSelector").addEventListener("click", function(){
            search( getFileId() ,  getTagTypeArgs() ,  getTargetSource() );
        });
    }

    function search(fileName,tagTypeArgs,targetSource){
         tagTypeAndSourceSelector(fileName,tagTypeArgs,targetSource);
    }

    function getFileId(){
        return file_id = getURLParameter('file_id');
    }

    function getTagTypeArgs(){
        var tagTypeArgs=[];
        var length = document.getElementsByTagName('label').length;
        for(var i= 0 ; i < length ; i++){
            var curTagLabel = document.getElementsByTagName('label')[i];
            var curTag;

            //label contain string "tag"
            if(curTagLabel.htmlFor.indexOf("tag") > -1){
                if(curTagLabel.childNodes[0].checked){
                    curTag = curTagLabel.childNodes[1].innerHTML.replace(/\s/g, '');
                    tagTypeArgs.push(curTag);
                }
            }
        }
        //console.log(tagTypeArgs);
        return tagTypeArgs;
    }

    function getTargetSource(){
        var targetSource = document.getElementById('sourceSelectorInfoInput').value;
        return targetSource;
    }
    
    //tag_type or source
    function tagTypeAndSourceSelector(fileName,tagTypeArgs, targetSource){
        //console.log(fileName +" 50");
        SidebarManager.loadTxt(fileName);
        $.ajax({
            type: "POST",      
            data:  { csrfmiddlewaretoken: getCookie('csrftoken'),
                     action : 'getTagContent',
                     file_name : fileName,
                    },

            success: function(response) { // on success..
              //console.log(response);
              var newResponse = [];
              var hasData = false;
              var index =0;
              for(var i = 0 ; i < response.length ; i++){
                var offset_start = (parseInt)(response[i]['fields']['offset_start']);
                var offset_end = (parseInt)(response[i]['fields']['offset_end']);
                var tag_type = response[i]['fields']['tag_type'];
                var source = response[i]['fields']['source'];

                if(tagTypeArgs.length ==0 &&  source === targetSource){
                    hasData = true;
                    var obj = new Object();
                    obj.offset_start = offset_start;
                    obj.offset_end = offset_end;
                    obj.tag_type = tag_type;
                    obj.source =  source;
                    var tag = {
                             "fields": obj
                    }
                    newResponse.push(tag);
                }
                
                for(var j = 0 ;  j < tagTypeArgs.length ; j++){
                    //console.log(tag_type +" " + tagTypeArgs[j]);
                    //console.log(source +" " + targetSource);
                    if(tag_type === tagTypeArgs[j] && source === targetSource){
                        //console.log("399");
                        hasData = true;
                        var obj = new Object();
                        obj.offset_start = offset_start;
                        obj.offset_end = offset_end;
                        obj.tag_type = tag_type;
                        obj.source =  source;
                        var tag = {
                             "fields": obj
                        }
                        newResponse.push(tag);
                        index++;
                        break;
                    } else if(tag_type === tagTypeArgs[j] && targetSource === ""){
                        //console.log("183");
                        hasData = true;
                        var obj = new Object();
                        obj.offset_start = offset_start;
                        obj.offset_end = offset_end;
                        obj.tag_type = tag_type;
                        obj.source =  source;
                        var tag = {
                             "fields": obj
                        }
                        newResponse.push(tag);
                        index++;
                        break;
                    }
                }
            }
              // console.log(hasData);
              if(hasData){
                 //  console.log(newResponse);
                   changeContent.change("tag", newResponse);
              }
            },

            error: function(err, x, req) { // on error..
              //  console.log('my message ' + err + " "+ req); 
            }
        });
    }
    function getURLParameter(name) {
          return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null
    }

    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
})();