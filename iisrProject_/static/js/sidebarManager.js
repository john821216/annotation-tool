var SidebarManager = (function(){
    var arrMenu;
    var newItem=[];
    var hasLoadingItem;
    var menuHasExpand = true;
    var levelIndex_from;
    var levelIndex_to;
    var rootSearchIndex;
    var curLevelSearchIndex;
    var maxLimitLoadingPerTime = 50;
    var hasMoreItems = false;
    var curSideBarItemCount = 0;

    return {
        ini : function (){
            $.getScript("../static/js/changeContent.js", function() {
                getSideBarChild();
                getFileId();
            });        
        },
        loadTxt : function(fileId){
            loadTxt(fileId);
        },
        loadTag : function(fileId){
            loadTag(fileId);
        },

        tagTypeAndSourceSelector:function(fileId,tagTypeArgs, targetSource ){
            tagTypeAndSourceSelector(fileId , tagTypeArgs , targetSource);
        }
    }

    function getSideBarChild(){
         getSideBarChildRootTitle();
    }

    function getSideBarChildRootTitle(){
        rootSearchIndex = getURLParameter('rootSearchIndex');

        if(rootSearchIndex == null){
            rootSearchIndex = 0 ;
        }

        //root title
        $.ajax({ // create an AJAX call...
            type: "POST",      
            data:  { csrfmiddlewaretoken: getCookie('csrftoken'),
                     action : 'getSideBarItem',
                     searchIndex : rootSearchIndex,
                   },           
            //dataType: "json",
            success: function(response) { // on success..
                rootTitle = response[0]['fields']['node_name'];
                getSideBarChildInformation();
            }
        });
    }

    function getSideBarChildInformation(){
        levelIndex_from = getURLParameter('index_from');
        levelIndex_to = getURLParameter('index_to');     
        curLevelSearchIndex = getURLParameter('searchIndex');       

        if(curLevelSearchIndex == null){
            curLevelSearchIndex= 0 ;
        }

        //search index
        $.ajax({ // create an AJAX call...
            type: "POST",      
            data:  { csrfmiddlewaretoken: getCookie('csrftoken'),
                     action : 'getSideBarItem',
                     searchIndex :  curLevelSearchIndex,
                   },
            //dataType: "json",
            success: function(response) { // on success..
                if(curLevelSearchIndex ==0){
                    levelIndex_from = response[0]['fields']['childIndex_from'];
                    levelIndex_to = response[0]['fields']['childIndex_to'];
                }

                arrMenu= [
                    { 
                        title: rootTitle,
                        id: 'root',
                        icon: 'fa fa-reorder',
                        items:[]
                    }
                ]
                iniMenu();
                document.getElementById("loading").style.display = "block";
                //console.log(levelIndex_to - levelIndex_from +"  "+  maxLimitLoadingPerTime)
                if(levelIndex_to - levelIndex_from >= maxLimitLoadingPerTime){
                    addChildItem(levelIndex_from,  parseInt(levelIndex_from) + 50);
                    levelIndex_from = parseInt(levelIndex_from)+ 51;
                    hasMoreItems = true;
                } else {
                    addChildItem(levelIndex_from,  levelIndex_to);
                    hasMoreItems = false;
                }
            },

            error: function(err, x, req) { // on error..
                //console.log('my message ' + err + " "+ req); 
            }
        });   
    }

    function getFileId(){
        var file_id = getURLParameter('file_id');
        //console.log(file_id);
        if(file_id != null && file_id != -1){
            localStorage.setItem('file_id' , file_id);
            loadTxt(file_id);
            loadTag(file_id);
        }
    }


    function addChildItem(index_from, index_to){
        $.ajax({
            type: "POST",      
            data:  { csrfmiddlewaretoken: getCookie('csrftoken'),
                        action : 'getSideBarItem',
                        searchIndex : index_from,
                    },
            //dataType: "json",
            success: function(response) { // on success..
                var childIndex_from = response[0]['fields']['childIndex_from'];
                var childIndex_to = response[0]['fields']['childIndex_to'];
                var chileSearchIndex = response[0]['fields']['searchIndex'];
                if(childIndex_from == -1 && childIndex_to == -1){
                    newItem.unshift(
                          {   name: response[0]['fields']['node_name'],
                              id: response[0]['fields']['node_name'],
                              link:  'index?index_from=' + levelIndex_from +'&index_to=' +levelIndex_to + '&searchIndex=' + chileSearchIndex +  '&rootSearchIndex=' + rootSearchIndex+ '&file_id=' + response[0]['fields']['file_name'],
                           })
                        ;
                } else {
                    newItem.unshift(
                              {   name: response[0]['fields']['node_name'],
                                  id: response[0]['fields']['node_name'],
                                  link:  'index?index_from=' + childIndex_from +'&index_to=' +childIndex_to + '&searchIndex=' + chileSearchIndex +  '&rootSearchIndex=' + chileSearchIndex + '&file_id=' + response[0]['fields']['file_name'],
                
                               }
                    )
                            ;
                }
                index_from++;
                if(index_from <= index_to ){
                    addChildItem(index_from , index_to);
                }

                //finish get data 
                else{
                    hasLoadingItem = true;
                    if(hasMoreItems){
                        newItem.unshift(
                              {   name: " Load More",
                                  id: 'loader',
                                  link:  '#',
                
                              })
                    }
                    addArrMenuToSideBar(index_to);      
                }
            }
        });
    }

    function addArrMenuToSideBar(childIndex_to){
        if (hasLoadingItem == false) {
            //console.log("REloading");
            window.setTimeout(function() {
                addArrMenuToSideBar(childIndex_to);
            }, 5);
        } else {
            hasLoadingItem = false;
            var addTo = $('#menu').multilevelpushmenu('activemenu'); 
            //console.log(curSideBarItemCount);
            $( '#menu' ).multilevelpushmenu( 'additems' , newItem , addTo , curSideBarItemCount  );
            curSideBarItemCount +=newItem.length-1;
            newItem=[];
            document.getElementById("loading").style.display = "none";
        }
    }

    function iniMenu() {      
        var href;
        var childIndex_from;
        var childIndex_to;
        $('#menu').multilevelpushmenu({
            menu :arrMenu,
            containersToPush: [$('#svg')],
            menuWidth: 200,
            onItemClick: function() {
                var id = arguments[2][0].getAttribute('id');
                if(id == 'loader'){
                    //remove items
                     var item = $( '#menu' ).multilevelpushmenu( 'finditemsbyname' , ' Load More' );
                    // console.log(item);
                     $( '#menu' ).multilevelpushmenu( 'removeitems' , item );

                    document.getElementById("loading").style.display = "block";
                    if(levelIndex_to - levelIndex_from >= maxLimitLoadingPerTime){
                        addChildItem(levelIndex_from,  parseInt(levelIndex_from) + 50);
                        levelIndex_from = parseInt(levelIndex_from)+ 51;
                        hasMoreItems = true;
                    } else {
                        addChildItem(levelIndex_from, levelIndex_to);
                        hasMoreItems = false;
                    }
                } else {
                   window.location.href =arguments[2][0].childNodes[0] ; 
                }
                
            },
            onGroupItemClick: function() {
                //console.log("186");
                document.getElementById("loading").style.display = "block";
                href = arguments[2][0].childNodes[0].getAttribute('href');
                childIndex_from = href.split("?")[0].split("=")[1];
                childIndex_to = href.split("?")[1].split("=")[1];
                hasloadingMenuFinishLoading = false;
                //console.log(childIndex_to +"  " +  hasExpand[childIndex_to]);
                if(hasExpand[childIndex_to] == 0){    
                    //document.getElementById("loading").style.display = "block";
                    addItem(childIndex_from , childIndex_to );
                    //console.log("145fff");
                } else {
                    //document.getElementById("loading").style.display = "block";
                }
            },
            onCollapseMenuStart: function() {
               // console.log("141");
                document.getElementById("loading").style.display = "block";
            },

            onCollapseMenuEnd: function() {
               // console.log("145");
                document.getElementById("loading").style.display = "none";
            },

            onExpandMenuStart: function() {

            },

            onExpandMenuEnd: function() {

            },
            onTitleItemClick: function() {
                //console.log(arguments);
            }
        });
    }

    function loadTxt(fileName) {
        $.ajax({ // create an AJAX call...
            type: "POST",      
            data:  { csrfmiddlewaretoken: getCookie('csrftoken'),
                     action : 'getTxtContent',
                     file_name : fileName,
                    },
            //dataType: "json",
            success: function(response) { // on success..
              //console.log(response);
              changeContent.change("text", response);
            },

            error: function(err, x, req) { // on error..
               // console.log('my message ' + err + " "+ req); 
            }
        });
    }

    function loadTag(fileName){
        $.ajax({
            type: "POST",      
            data:  { csrfmiddlewaretoken: getCookie('csrftoken'),
                     action : 'getTagContent',
                     file_name : fileName,
                    },

            success: function(response) { // on success..
             // console.log(response);
              changeContent.change("tag", response);
            },

            error: function(err, x, req) { // on error..
              //  console.log('my message ' + err + " "+ req); 
            }
        });
    }


    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    function getURLParameter(name) {
          return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null
    }
})();
